﻿using Bastion.Senders.SmsSender.Abstractions;
using Bastion.Senders.TwilioSender;
using Bastion.Senders.TwilioSender.Options;

using Microsoft.Extensions.Configuration;

using System;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Contains extension methods to <see cref="IServiceCollection"/> for configuring twilio sms sender.
    /// </summary>
    public static class TwilioSmsSenderServiceCollectionExtensions
    {
        /// <summary>
        /// Adds twilio sms sender.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/> to add services to.</param>
        /// <param name="setupAction">An action to configure the <see cref="TwilioOptions"/>.</param>
        /// <returns>The <see cref="IServiceCollection"/> so that additional calls can be chained.</returns>
        public static IServiceCollection AddTwilioSmsSender(this IServiceCollection services, Action<TwilioOptions> setupAction)
        {
            services.AddTwilioSmsSender();

            if (setupAction != null)
            {
                services.Configure(setupAction);
            }

            return services;
        }

        /// <summary>
        /// Adds twilio sms sender.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/> to add services to.</param>
        /// <param name="configuration">The service configuration.</param>
        /// <returns>The <see cref="IServiceCollection"/> so that additional calls can be chained.</returns>
        public static IServiceCollection AddTwilioSmsSender(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTwilioSmsSender();
            services.Configure<TwilioOptions>(configuration);

            return services;
        }

        /// <summary>
        /// Adds twilio sms sender.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/> to add services to.</param>
        /// <returns>The <see cref="IServiceCollection"/> so that additional calls can be chained.</returns>
        public static IServiceCollection AddTwilioSmsSender(this IServiceCollection services)
        {
            services.AddHttpClient<ISmsSender, TwilioSmsSender>();

            return services;
        }
    }
}