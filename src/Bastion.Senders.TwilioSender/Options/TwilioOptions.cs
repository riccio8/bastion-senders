﻿using Bastion.Senders.SmsSender.Abstractions.Options;

namespace Bastion.Senders.TwilioSender.Options
{
    /// <summary>
    /// Twilio options.
    /// </summary>
    public class TwilioOptions : SmsSenderOptions
    {
        /// <summary>
        /// Gets or sets account SID.
        /// </summary>
        public string AccountSID { get; set; }

        /// <summary>
        /// Gets or sets auth token.
        /// </summary>
        public string AuthToken { get; set; }

        /// <summary>
        /// Gets or sets Messaging Service SID.
        /// </summary>
        public string MessagingServiceSID { get; set; }

        /// <summary>
        /// Gets or sets Verification Service SID.
        /// </summary>
        public string VerificationServiceSID { get; set; }
    }
}