﻿using Bastion.Senders.Abstractions;
using Bastion.Senders.SmsSender.Abstractions;
using Bastion.Senders.TwilioSender.Options;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using System;
using System.Net.Http;
using System.Threading.Tasks;

using Twilio.Clients;
using Twilio.Exceptions;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Rest.Lookups.V1;
using Twilio.Rest.Verify.V2.Service;
using Twilio.Types;

namespace Bastion.Senders.TwilioSender
{
    /// <summary>
    /// Twilio sms sender.
    /// </summary>
    public class TwilioSmsSender : SmsSenderBase
    {
        private readonly string messagingServiceSid;
        private readonly string verificationServiceSid;

        private readonly PhoneNumber fromPhoneNumber;
        private readonly ITwilioRestClient twilioRestClient;

        /// <summary>
        /// Initializes a new instance of <see cref="TwilioSmsSender"/>.
        /// </summary>
        public TwilioSmsSender(
            HttpClient httpClient,
            ILogger<ISmsSender> logger,
            IOptions<TwilioOptions> optionsTwilio)
            : base(logger, optionsTwilio)
        {
            if (optionsSmsSender.Enabled)
            {
                if (string.IsNullOrEmpty(optionsTwilio.Value.AccountSID))
                {
                    logger.LogWarning("Twilio, send sms disabled, 'AccountSID' in configuration not found.");
                    optionsSmsSender.Enabled = false;
                }
                else
                {
                    if (string.IsNullOrEmpty(optionsTwilio.Value.AuthToken))
                    {
                        logger.LogWarning("Twilio, send sms disabled, 'AuthToken' in configuration not found.");
                        optionsSmsSender.Enabled = false;
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(optionsSmsSender.From))
                        {
                            logger.LogWarning("Twilio, send sms disabled, 'From' in configuration not found.");
                            optionsSmsSender.Enabled = false;
                        }
                        else
                        {
                            messagingServiceSid = optionsTwilio.Value.MessagingServiceSID;
                            verificationServiceSid = optionsTwilio.Value.VerificationServiceSID;

                            fromPhoneNumber = new PhoneNumber(optionsSmsSender.From);
                            try
                            {
                                twilioRestClient = new TwilioRestClient(
                                    optionsTwilio.Value.AccountSID,
                                    optionsTwilio.Value.AuthToken,
                                    httpClient: new Twilio.Http.SystemNetHttpClient(httpClient));
                            }
                            catch (Exception ex)
                            {
                                logger.LogWarning($"Twilio, initialize error: {ex.Message}");
                                optionsSmsSender.Enabled = false;
                            }
                        }
                    }
                }
            }
            else
            {
                logger.LogWarning("Twilio, send sms disabled from configuration.");
            }
        }

        /// <summary>
        /// Send sms.
        /// </summary>
        /// <param name="phoneTo">To phone number.</param>
        /// <param name="textMessage">Plain text message.</param>
        public override async Task<SendersResult> SendSmsAsync(string phoneTo, string textMessage)
        {
            if (optionsSmsSender.Enabled)
            {
                logger.LogInformation($"Twilio, Sms sending to: {phoneTo}");

                try
                {
                    var response = await MessageResource.CreateAsync(
                        body: textMessage,
                        client: twilioRestClient,
                        to: new PhoneNumber(phoneTo),
                        messagingServiceSid: messagingServiceSid,
                        from: string.IsNullOrEmpty(messagingServiceSid) ? fromPhoneNumber : default);

                    if (!response.ErrorCode.HasValue)
                    {
                        logger.LogInformation($"Twilio, Sms send to: {phoneTo}");
                        return SendersResult.Success;
                    }
                    else
                    {
                        var failedMessage = $"Twilio, Error when Sms sending to: {phoneTo}, code: {response.ErrorCode}, message: {response.ErrorMessage}";
                        logger.LogWarning(failedMessage);
                        return SendersResult.Failed(failedMessage);
                    }
                }
                catch (ApiException ex)
                {
                    var failedMessage = $"Twilio, Error when Sms sending to: {phoneTo}, Error: {ex.Code} - {ex.MoreInfo}, Exception: {ex.Message}";
                    logger.LogWarning(failedMessage);
                    return SendersResult.Failed(failedMessage);
                }
                catch (Exception ex)
                {
                    var failedMessage = $"Twilio, Error when Sms sending to: {phoneTo}, Exception: {ex.Message}";
                    logger.LogWarning(failedMessage);
                    return SendersResult.Failed(failedMessage);
                }
            }
            return await base.SendSmsAsync(phoneTo, textMessage);
        }

        /// <summary>
        /// Verification phone number.
        /// </summary>
        /// <param name="phone">Phone number.</param>
        public override async Task<SendersResult> VerificationPhoneNumberAsync(string phone)
        {
            if (optionsSmsSender.Enabled)
            {
                logger.LogInformation($"Twilio, Sms sending to: {phone}");

                try
                {
                    var response = await VerificationResource.CreateAsync(
                        to: phone,
                        channel: "sms",
                        client: twilioRestClient,
                        pathServiceSid: verificationServiceSid);

                    if (response.Valid.HasValue && response.Valid.Value)
                    {
                        return SendersResult.Success;
                    }
                    else
                    {
                        var failedMessage = $"Twilio, Error when verification phone number: {phone}, sid: {response.Sid}";
                        logger.LogWarning(failedMessage);
                        return SendersResult.Failed(failedMessage);
                    }
                }
                catch (ApiException ex)
                {
                    var failedMessage = $"Twilio, Error when verification phone number: {phone}, Error: {ex.Code} - {ex.MoreInfo}, Exception: {ex.Message}";
                    logger.LogWarning(failedMessage);
                    return SendersResult.Failed(failedMessage);
                }
                catch (Exception ex)
                {
                    var failedMessage = $"Twilio, Error when verification phone number: {phone}, Exception: {ex.Message}";
                    logger.LogWarning(failedMessage);
                    return SendersResult.Failed(failedMessage);
                }
            }

            return await base.VerificationPhoneNumberAsync(phone);
        }

        /// <summary>
        /// Validation phone number.
        /// </summary>
        /// <param name="phone">Phone.</param>
        public override async Task<PhoneNumberInfo> ValidationPhoneNumberAsync(string phone)
        {
            var response = await PhoneNumberResource.FetchAsync(
                client: twilioRestClient,
                pathPhoneNumber: new PhoneNumber(phone));

            return new PhoneNumberInfo
            {
                CountryCode = response?.CountryCode,
                GlobalFormat = response?.PhoneNumber?.ToString(),
                NationalFormat = response?.NationalFormat,
            };
        }
    }
}