﻿namespace Bastion.Senders.PushSender.Abstractions.Options
{
    /// <summary>
    /// Push-notification sender options.
    /// </summary>
    public class PushSenderOptions
    {
        /// <summary>
        /// Gets or sets Enabled.
        /// </summary>
        public bool Enabled { get; set; } = false;
    }
}