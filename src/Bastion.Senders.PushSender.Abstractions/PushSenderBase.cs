﻿using Bastion.Senders.PushSender.Abstractions.Options;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Bastion.Senders.PushSender.Abstractions
{
    /// <summary>
    /// Push-notification Sender base.
    /// </summary>
    public abstract class PushSenderBase
    {
        /// <summary>
        /// Logger.
        /// </summary>
        protected readonly ILogger<IPushSender> logger;

        /// <summary>
        /// Options push-notification sender.
        /// </summary>
        protected readonly PushSenderOptions optionsPushSender;

        /// <summary>
        /// Initializes a new instance of <see cref="PushSenderBase"/>.
        /// </summary>
        protected PushSenderBase(
            ILogger<IPushSender> logger,
            IOptions<PushSenderOptions> optionsPushSender)
        {
            this.logger = logger;
            this.optionsPushSender = optionsPushSender.Value;
        }
    }
}