﻿using System.Text.Json.Serialization;

namespace Bastion.Senders.PushSender.Messages
{
    /// <summary>
    /// Push Notification data base.
    /// </summary>
    public abstract class PushNotificationDataBase
    {
        /// <summary>
        /// Click action.
        /// </summary>
        [JsonPropertyName("click_action")]
        public string ClickAction { get; set; } = "FLUTTER_NOTIFICATION_CLICK";

        /// <summary>
        /// Type of notification.
        /// </summary>
        [JsonPropertyName("type")]
        public string TypeNotification { get; set; } = "PUSH_NOTIFICATION";
    }
}