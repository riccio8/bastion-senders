﻿using System.Text.Json.Serialization;

namespace Bastion.Senders.PushSender.Messages
{
    /// <summary>
    /// Push notification message data.
    /// </summary>
    public sealed class PushNotificationMessageData : PushNotificationDataBase
    {
        /// <summary>
        /// Message.
        /// </summary>
        [JsonPropertyName("message")]
        public string Message { get; set; }
    }
}