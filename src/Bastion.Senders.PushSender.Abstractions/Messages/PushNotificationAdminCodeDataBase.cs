﻿using System.Text.Json.Serialization;

namespace Bastion.Senders.PushSender.Messages
{
    /// <summary>
    /// Push notification admin code data base.
    /// </summary>
    public abstract class PushNotificationAdminCodeDataBase : PushNotificationDataBase
    {
        /// <summary>
        /// Type of notification.
        /// </summary>
        [JsonPropertyName("type")]
        public new string TypeNotification { get; set; } = "ADMIN_CODE";
    }
}