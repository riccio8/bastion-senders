﻿using Bastion.Senders.Abstractions;
using Bastion.Senders.PushSender.Messages;

using System.Threading.Tasks;

namespace Bastion.Senders.PushSender.Abstractions
{
    /// <summary>
    /// Interface push-notification sender.
    /// </summary>
    public interface IPushSender
    {
        /// <summary>
        /// Send data notification.
        /// </summary>
        /// <param name="toDevice">This parameter specifies the recipient of a message.</param>
        /// <param name="title">Title.</param>
        /// <param name="subject">Subject.</param>
        /// <param name="data">Data.</param>
        /// <param name="channelId">Channel id.</param>
        Task<SendersResult> SendPushNotificationAsync<TFCMNotificationData>(
            string toDevice,
            string title,
            string subject,
            TFCMNotificationData data,
            string channelId = default)
            where TFCMNotificationData : PushNotificationDataBase;

        /// <summary>
        /// Send push-notification.
        /// </summary>
        /// <param name="toDevice">This parameter specifies the recipient of a message.</param>
        /// <param name="title">Title.</param>
        /// <param name="subject">Subject.</param>
        /// <param name="textMessage">Plain text message.</param>
        /// <param name="channelId">Channel id.</param>
        Task<SendersResult> SendPushNotificationAsync(
            string toDevice,
            string title,
            string subject,
            string textMessage,
            string channelId = default);
    }
}