﻿namespace Bastion.Senders.SmsSender.Abstractions
{
    /// <summary>
    /// Phone number.
    /// </summary>
    public class PhoneNumberInfo
    {
        /// <summary>
        /// Country code.
        /// </summary>
        public string CountryCode { get; set; }

        /// <summary>
        /// National format.
        /// </summary>
        public string NationalFormat { get; set; }

        /// <summary>
        /// Global format.
        /// </summary>
        public string GlobalFormat { get; set; }
    }
}