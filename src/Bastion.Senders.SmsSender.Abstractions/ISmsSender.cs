﻿using Bastion.Senders.Abstractions;

using System.Threading.Tasks;

namespace Bastion.Senders.SmsSender.Abstractions
{
    /// <summary>
    /// Interface sms sender.
    /// </summary>
    public interface ISmsSender
    {
        /// <summary>
        /// Send sms.
        /// </summary>
        /// <param name="phoneTo">To phone number.</param>
        /// <param name="textMessage">Plain text message.</param>
        Task<SendersResult> SendSmsAsync(string phoneTo, string textMessage);

        /// <summary>
        /// Verification phone number.
        /// </summary>
        /// <param name="phone">Phone number.</param>
        Task<SendersResult> VerificationPhoneNumberAsync(string phone);

        /// <summary>
        /// Validation phone number.
        /// </summary>
        /// <param name="phone">Phone number.</param>
        Task<PhoneNumberInfo> ValidationPhoneNumberAsync(string phone);
    }
}