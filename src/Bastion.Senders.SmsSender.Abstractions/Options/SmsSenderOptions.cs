﻿namespace Bastion.Senders.SmsSender.Abstractions.Options
{
    /// <summary>
    /// Sms sender options.
    /// </summary>
    public class SmsSenderOptions
    {
        /// <summary>
        /// Gets or sets Enabled.
        /// </summary>
        public bool Enabled { get; set; } = false;

        /// <summary>
        /// Gets or sets From phone number.
        /// </summary>
        public string From { get; set; }
    }
}