﻿using Bastion.Senders.Abstractions;
using Bastion.Senders.SmsSender.Abstractions.Options;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using System.Threading.Tasks;

namespace Bastion.Senders.SmsSender.Abstractions
{
    /// <summary>
    /// Sms Sender base.
    /// </summary>
    public abstract class SmsSenderBase : ISmsSender
    {
        /// <summary>
        /// Logger.
        /// </summary>
        protected readonly ILogger<ISmsSender> logger;

        /// <summary>
        /// Options sms sender.
        /// </summary>
        protected readonly SmsSenderOptions optionsSmsSender;

        /// <summary>
        /// Initializes a new instance of <see cref="SmsSenderBase"/>.
        /// </summary>
        protected SmsSenderBase(
            ILogger<ISmsSender> logger,
            IOptions<SmsSenderOptions> optionsSmsSender)
        {
            this.logger = logger;
            this.optionsSmsSender = optionsSmsSender.Value;
        }

        /// <summary>
        /// Send sms.
        /// </summary>
        /// <param name="phoneTo">To phone number.</param>
        /// <param name="textMessage">Plain text message.</param>
        public virtual Task<SendersResult> SendSmsAsync(string phoneTo, string textMessage)
        {
            logger.LogInformation($"Twilio, Skip sms sending to: {phoneTo}");

            return Task.FromResult(SendersResult.Skip);
        }

        /// <summary>
        /// Validation phone number.
        /// </summary>
        /// <param name="phone">Phone number.</param>
        public abstract Task<PhoneNumberInfo> ValidationPhoneNumberAsync(string phone);

        /// <summary>
        /// Verification phone number.
        /// </summary>
        /// <param name="phone">Phone number.</param>
        public virtual Task<SendersResult> VerificationPhoneNumberAsync(string phone)
        {
            return Task.FromResult(SendersResult.Skip);
        }
    }
}