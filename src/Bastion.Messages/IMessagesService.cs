﻿using Bastion.Messages.Models;
using Bastion.Messages.Stores.Models;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bastion.Messages
{
    /// <summary>
    /// Interface messages service.
    /// </summary>
    public interface IMessagesService
    {
        /// <summary>
        /// Get message for email.
        /// </summary>
        /// <param name="messageId">Message id</param>
        /// <param name="languageId">Language id.</param>
        /// <returns>Task of message.</returns>
        Task<MessageEmail> GetMessageForEmailAsync(string messageId, string languageId);

        /// <summary>
        /// Get message for email with parameters.
        /// </summary>
        /// <param name="messageId">Message id</param>
        /// <param name="languageId">Language id.</param>
        /// <param name="objectWithParameters">Object with parameters.</param>
        /// <returns>Task of message.</returns>
        Task<MessageEmail> GetMessageForEmailAsync<T>(string messageId, string languageId, T objectWithParameters) where T : class;

        /// <summary>
        /// Get message for sms.
        /// </summary>
        /// <param name="messageId">Message id</param>
        /// <param name="languageId">Language id.</param>
        /// <returns>Task of message.</returns>
        Task<MessageSms> GetMessageForSmsAsync(string messageId, string languageId);

        /// <summary>
        /// Get message for sms with parameters.
        /// </summary>
        /// <param name="messageId">Message id</param>
        /// <param name="languageId">Language id.</param>
        /// <param name="objectWithParameters">Object with parameters.</param>
        /// <returns>Task of message.</returns>
        Task<MessageSms> GetMessageForSmsAsync<T>(string messageId, string languageId, T objectWithParameters) where T : class;

        /// <summary>
        /// Get message for push-notification.
        /// </summary>
        /// <param name="messageId">Message id</param>
        /// <param name="languageId">Language id.</param>
        /// <returns>Task of message.</returns>
        Task<MessagePush> GetMessageForPushAsync(string messageId, string languageId);

        /// <summary>
        /// Get message for push-notification with parameters.
        /// </summary>
        /// <param name="messageId">Message id</param>
        /// <param name="languageId">Language id.</param>
        /// <param name="objectWithParameters">Object with parameters.</param>
        /// <returns>Task of message.</returns>
        Task<MessagePush> GetMessageForPushAsync<T>(string messageId, string languageId, T objectWithParameters) where T : class;

        /// <summary>
        /// Get global find and replace values.
        /// </summary>
        /// <returns>The list of find and replace values.</returns>
        IReadOnlyList<FindReplace> GetGlobalFindReplace();
    }
}