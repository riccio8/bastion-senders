﻿using Bastion.Messages.Stores.Models;

using System.Collections.Generic;

namespace Bastion.Messages.Comparers
{
    public class FindReplaceComparer : EqualityComparer<FindReplace>
    {
        public override bool Equals(FindReplace x, FindReplace y)
        {
            return string.Equals(x?.Find, y?.Find);
        }

        public override int GetHashCode(FindReplace obj)
        {
            return obj?.Find?.GetHashCode() ?? 0;
        }
    }
}