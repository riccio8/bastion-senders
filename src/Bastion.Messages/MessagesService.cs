﻿using Bastion.Messages.Comparers;
using Bastion.Messages.Extensions;
using Bastion.Messages.Models;
using Bastion.Messages.Options;
using Bastion.Messages.Stores;
using Bastion.Messages.Stores.Models;

using Microsoft.Extensions.Options;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bastion.Messages
{
    /// <summary>
    /// Messages service.
    /// </summary>
    public class MessagesService : IMessagesService
    {
        private readonly MessagesOptions messagesOptions;
        private readonly IMessagesRepository messageRepository;
        private readonly IReadOnlyList<FindReplace> globalFindReplaceValues;

        /// <summary>
        /// Initializes a new instance of <see cref="MessagesService"/>.
        /// </summary>
        public MessagesService(
            IMessagesRepository messageRepository,
            IOptions<MessagesOptions> messagesOptions)
        {
            this.messageRepository = messageRepository;
            this.messagesOptions = messagesOptions.Value;
            globalFindReplaceValues = this.messagesOptions?.FindReplaceValues?.ToFindReplaceValues()?.AsReadOnly();
        }

        /// <summary>
        /// Get global find and replace values.
        /// </summary>
        /// <returns>The list of find and replace values.</returns>
        public IReadOnlyList<FindReplace> GetGlobalFindReplace()
        {
            return globalFindReplaceValues;
        }

        /// <summary>
        /// Get message for email.
        /// </summary>
        /// <param name="messageId">Message id</param>
        /// <returns>Task of message.</returns>
        public Task<MessageEmail> GetMessageForEmailAsync(
            string messageId,
            string languageId)
        {
            return GetMessageEmailAsync(messageId, languageId, default);
        }

        /// <summary>
        /// Get message for email with parameters.
        /// </summary>
        /// <param name="messageId">Message id</param>
        /// <param name="objectWithParameters">Object with parameters.</param>
        /// <returns>Task of message.</returns>
        public Task<MessageEmail> GetMessageForEmailAsync<T>(
            string messageId,
            string languageId,
            T objectWithParameters) where T : class
        {
            return GetMessageEmailAsync(messageId, languageId, objectWithParameters.ToFindReplaceValues());
        }

        /// <summary>
        /// Get message for sms.
        /// </summary>
        /// <param name="messageId">Message id</param>
        /// <returns>Task of message.</returns>
        public Task<MessageSms> GetMessageForSmsAsync(
            string messageId,
            string languageId)
        {
            return GetMessageSmsAsync(messageId, languageId, default);
        }

        /// <summary>
        /// Get message for sms with parameters.
        /// </summary>
        /// <param name="messageId">Message id</param>
        /// <param name="objectWithParameters">Object with parameters.</param>
        /// <returns>Task of message.</returns>
        public Task<MessageSms> GetMessageForSmsAsync<T>(
            string messageId,
            string languageId,
            T objectWithParameters) where T : class
        {
            return GetMessageSmsAsync(messageId, languageId, objectWithParameters.ToFindReplaceValues());
        }

        /// <summary>
        /// Get message for push-notification.
        /// </summary>
        /// <param name="messageId">Message id</param>
        /// <param name="languageId">Language id.</param>
        /// <returns>Task of message.</returns>
        public Task<MessagePush> GetMessageForPushAsync(
            string messageId,
            string languageId)
        {
            return GetMessagePushAsync(messageId, languageId, default);
        }

        /// <summary>
        /// Get message for push-notification with parameters.
        /// </summary>
        /// <param name="messageId">Message id</param>
        /// <param name="languageId">Language id.</param>
        /// <param name="objectWithParameters">Object with parameters.</param>
        /// <returns>Task of message.</returns>
        public Task<MessagePush> GetMessageForPushAsync<T>(
            string messageId,
            string languageId,
            T objectWithParameters) where T : class
        {
            return GetMessagePushAsync(messageId, languageId, objectWithParameters.ToFindReplaceValues());
        }

        public async Task<MessageEmail> GetMessageEmailAsync(string messageId, string languageId, List<FindReplace> localFindReplaceValues)
        {
            var message = await GetMessageAsync(messageId, languageId);
            var values = await GetValuesAsync(localFindReplaceValues);

            var templateId = message.TemplateId ?? messagesOptions.DefaultTemplateId;
            var template = await messageRepository.GetTemplateByIdAsync(templateId, message.LanguageId);

            return message.ToEmail(template, values);
        }

        public async Task<MessageSms> GetMessageSmsAsync(string messageId, string languageId, List<FindReplace> localFindReplaceValues)
        {
            var message = await GetMessageAsync(messageId, languageId);
            var values = await GetValuesAsync(localFindReplaceValues);

            return message.ToSms(values);
        }

        public async Task<MessagePush> GetMessagePushAsync(string messageId, string languageId, List<FindReplace> localFindReplaceValues)
        {
            var message = await GetMessageAsync(messageId, languageId);
            var values = await GetValuesAsync(localFindReplaceValues);

            return message.ToPush(values);
        }

        public async Task<Message> GetMessageAsync(string messageId, string languageId)
        {
            var message = await messageRepository.GetMessageByIdAsync(messageId, languageId)
                ?? await messageRepository.GetMessageByIdAsync(messageId, messagesOptions.DefaultLanguageId)
                ?? throw new Exception($"Message with id: '{messageId}' and language: '{languageId}' not extit.");

            return message;
        }

        public async Task<List<FindReplace>> GetValuesAsync(List<FindReplace> localFindReplaceValues)
        {
            var values = new HashSet<FindReplace>(new FindReplaceComparer());
            values.AddRange(localFindReplaceValues);
            values.AddRange(globalFindReplaceValues.ToList());
            values.AddRange(await messageRepository.GetFindReplaceAsync());

            return values.ToList();
        }
    }
}