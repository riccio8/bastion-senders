﻿using Bastion.Messages.Models;
using Bastion.Messages.Stores.Models;

using System.Collections.Generic;

namespace Bastion.Messages.Extensions
{
    internal static class MessageExtensions
    {
        public static MessageEmail ToEmail(this Message message, Template template, List<FindReplace> values)
        {
            return new MessageEmail
            {
                Subject = string.IsNullOrEmpty(message.Subject) ? default : message.Subject.Replace(values),
                Text = string.IsNullOrEmpty(message.ContextText) ? default : message.ContextText.Replace(values),
                Html = string.IsNullOrEmpty(message.ContextHtml) ? default : (template?.Header + message.ContextHtml + template?.Footer).Replace("{PreView}", message.PreView).Replace(values),
            };
        }

        public static MessagePush ToPush(this Message message, List<FindReplace> values)
        {
            return new MessagePush
            {
                Title = string.IsNullOrEmpty(message.PreView) ? default : message.PreView.Replace(values),
                Subject = string.IsNullOrEmpty(message.Subject) ? default : message.Subject.Replace(values),
                Text = string.IsNullOrEmpty(message.ContextText) ? default : message.ContextText.Replace(values),
            };
        }

        public static MessageSms ToSms(this Message message, List<FindReplace> values)
        {
            return new MessageSms
            {
                Text = string.IsNullOrEmpty(message.ContextSms) ? default : message.ContextSms.Replace(values),
            };
        }

        private static string Replace(this string source, List<FindReplace> values)
        {
            if (string.IsNullOrEmpty(source))
            {
                return source;
            }

            values.ForEach(x => source = source.Replace(x.Find, x.Replace));

            return source;
        }
    }
}