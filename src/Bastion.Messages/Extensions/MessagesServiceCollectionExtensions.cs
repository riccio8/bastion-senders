﻿using Bastion.Messages;
using Bastion.Messages.Options;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection.Extensions;

using System;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Contains extension methods to <see cref="IServiceCollection"/> for configuring messages service.
    /// </summary>
    public static class MessagesServiceCollectionExtensions
    {
        /// <summary>
        /// Adds messages service.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/> to add services to.</param>
        /// <param name="setupAction">An action to configure the <see cref="MessagesOptions"/>.</param>
        /// <returns>The <see cref="IServiceCollection"/> so that additional calls can be chained.</returns>
        public static IServiceCollection AddMessagesService(this IServiceCollection services, Action<MessagesOptions> setupAction)
        {
            services.AddMessagesService();

            if (setupAction != null)
            {
                services.Configure(setupAction);
            }
            return services;
        }

        /// <summary>
        /// Adds messages service.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/> to add services to.</param>
        /// <param name="configuration">The service configuration.</param>
        /// <returns>The <see cref="IServiceCollection"/> so that additional calls can be chained.</returns>
        public static IServiceCollection AddMessagesService(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddMessagesService();

            services.Configure<MessagesOptions>(configuration);

            return services;
        }

        /// <summary>
        /// Adds messages service.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/> to add services to.</param>
        /// <returns>The <see cref="IServiceCollection"/> so that additional calls can be chained.</returns>
        public static IServiceCollection AddMessagesService(this IServiceCollection services)
        {
            services.TryAddScoped<IMessagesService, MessagesService>();

            return services;
        }
    }
}