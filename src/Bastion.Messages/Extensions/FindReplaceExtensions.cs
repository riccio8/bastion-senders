﻿using Bastion.Messages.Stores.Models;

using System.Collections.Generic;
using System.Linq;

namespace Bastion.Messages.Extensions
{
    internal static class FindReplaceExtensions
    {
        public static List<FindReplace> ToFindReplaceValues<T>(this T source) where T : class
        {
            return typeof(T).GetProperties().Select(propInfo => new FindReplace
            {
                Find = '{' + propInfo.Name + '}',
                Replace = propInfo.GetValue(source)?.ToString()
            }).Where(x => !string.IsNullOrEmpty(x.Replace)).ToList();
        }

        public static List<FindReplace> ToFindReplaceValues(this IDictionary<string, string> source)
        {
            return source?.Select(x => new FindReplace { Find = ToFind(x.Key), Replace = x.Value }).ToList();
        }

        public static HashSet<FindReplace> AddRange(this HashSet<FindReplace> source, List<FindReplace> values)
        {
            if (values?.Count > 0)
            {
                values.ForEach(x => source.Add(x));
            }
            return source;
        }

        private static string ToFind(this string source)
        {
            if (source?.Length > 0)
            {
                if (source[0] != '{')
                {
                    source = '{' + source;
                }
                if (source[source.Length - 1] != '}')
                {
                    source += '}';
                }
            }
            return source;
        }
    }
}