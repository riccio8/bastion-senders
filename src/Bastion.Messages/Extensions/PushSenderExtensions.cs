﻿using Bastion.Messages.Models;
using Bastion.Senders.Abstractions;
using Bastion.Senders.PushSender.Abstractions;

using System.Threading.Tasks;

namespace Bastion.Messages.Extensions
{
    /// <summary>
    /// Push sender extensions.
    /// </summary>
    public static class PushSenderExtensions
    {
        /// <summary>
        /// Send push-notification from message service.
        /// </summary>
        /// <param name="pushSender">Push sender.</param>
        /// <param name="deviceId">To device id.</param>
        /// <param name="messagesService">Messages service.</param>
        /// <param name="messageId">Message id.</param>
        /// <param name="languageId">Language id.</param>
        /// <returns>The task.</returns>
        public static async Task<SendersResult> SendPushNotificationAsync(
            this IPushSender pushSender,
            string deviceId,
            IMessagesService messagesService,
            string messageId,
            string languageId)
        {
            var message = await messagesService.GetMessageForPushAsync(messageId, languageId);

            return await pushSender.SendPushNotificationAsync(deviceId, message);
        }

        /// <summary>
        /// Send push-notification from message service with parameters.
        /// </summary>
        /// <param name="pushSender">Push sender.</param>
        /// <param name="deviceId">To device id.</param>
        /// <param name="messagesService">Messages service.</param>
        /// <param name="messageId">Message id.</param>
        /// <param name="languageId">Language id.</param>
        /// <param name="objectWithParameters">Object with parameters.</param>
        /// <returns>The task.</returns>
        public static async Task<SendersResult> SendPushNotificationAsync<T>(
            this IPushSender pushSender,
            string deviceId,
            IMessagesService messagesService,
            string messageId,
            string languageId,
            T objectWithParameters) where T : class
        {
            var message = await messagesService.GetMessageForPushAsync(messageId, languageId, objectWithParameters);

            return await pushSender.SendPushNotificationAsync(deviceId, message);
        }

        private static Task<SendersResult> SendPushNotificationAsync(
            this IPushSender pushSender,
            string deviceId,
            MessagePush message)
        {
            return pushSender.SendPushNotificationAsync(deviceId, message.Title, message.Subject, message.Text);
        }
    }
}