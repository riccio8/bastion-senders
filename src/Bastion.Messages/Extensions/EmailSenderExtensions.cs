﻿using Bastion.Messages.Models;
using Bastion.Senders.Abstractions;
using Bastion.Senders.EmailSender.Abstractions;

using System.Threading.Tasks;

namespace Bastion.Messages.Extensions
{
    /// <summary>
    /// Email sender extensions.
    /// </summary>
    public static class EmailSenderExtensions
    {
        /// <summary>
        /// Send email from message service.
        /// </summary>
        /// <param name="emailSender">Email sender.</param>
        /// <param name="emailTo">To email address.</param>
        /// <param name="messagesService">Messages service.</param>
        /// <param name="messageId">Message id.</param>
        /// <param name="languageId">Language id.</param>
        /// <returns>The task.</returns>
        public static async Task<SendersResult> SendEmailAsync(
            this IEmailSender emailSender,
            string emailTo,
            IMessagesService messagesService,
            string messageId,
            string languageId)
        {
            var message = await messagesService.GetMessageForEmailAsync(messageId, languageId);

            return await emailSender.SendEmailAsync(emailTo, message);
        }

        /// <summary>
        /// Send email from message service with parameters.
        /// </summary>
        /// <param name="emailSender">Email sender.</param>
        /// <param name="emailTo">To email address.</param>
        /// <param name="messagesService">Messages service.</param>
        /// <param name="messageId">Message id.</param>
        /// <param name="languageId">Language id.</param>
        /// <param name="objectWithParameters">Object with parameters.</param>
        /// <returns>The task.</returns>
        public static async Task<SendersResult> SendEmailAsync<T>(
            this IEmailSender emailSender,
            string emailTo,
            IMessagesService messagesService,
            string messageId,
            string languageId,
            T objectWithParameters) where T : class
        {
            var message = await messagesService.GetMessageForEmailAsync(messageId, languageId, objectWithParameters);

            return await emailSender.SendEmailAsync(emailTo, message);
        }

        private static Task<SendersResult> SendEmailAsync(
            this IEmailSender emailSender,
            string emailTo,
            MessageEmail message)
        {
            return emailSender.SendEmailAsync(emailTo, message.Subject, message.Html, message.Text);
        }
    }
}