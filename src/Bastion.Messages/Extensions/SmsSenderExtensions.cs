﻿using Bastion.Messages;
using Bastion.Messages.Models;
using Bastion.Senders.Abstractions;
using Bastion.Senders.SmsSender.Abstractions;

using System.Threading.Tasks;

namespace Bastion.Senders.SmsSender.Extensions
{
    /// <summary>
    /// Sms sender extensions.
    /// </summary>
    public static class SmsSenderExtensions
    {
        /// <summary>
        /// Send sms from message service.
        /// </summary>
        /// <param name="smsSender">Sms sender.</param>
        /// <param name="phoneTo">To phone number.</param>
        /// <param name="messagesService">Messages service.</param>
        /// <param name="messageId">Message id.</param>
        /// <param name="languageId">Language id.</param>
        /// <returns>The task.</returns>
        public static async Task<SendersResult> SendSmsAsync(
            this ISmsSender smsSender,
            string phoneTo,
            IMessagesService messagesService,
            string messageId,
            string languageId)
        {
            var message = await messagesService.GetMessageForSmsAsync(messageId, languageId);

            return await smsSender.SendSmsAsync(phoneTo, message);
        }

        /// <summary>
        /// Send sms from message service with parameters.
        /// </summary>
        /// <param name="smsSender">Sms sender.</param>
        /// <param name="phoneTo">To phone number.</param>
        /// <param name="messagesService">Messages service.</param>
        /// <param name="messageId">Message id.</param>
        /// <param name="languageId">Language id.</param>
        /// <param name="objectWithParameters">Object with parameters.</param>
        /// <returns>The task.</returns>
        public static async Task<SendersResult> SendSmsAsync<T>(
            this ISmsSender smsSender,
            string phoneTo,
            IMessagesService messagesService,
            string messageId,
            string languageId,
            T objectWithParameters) where T : class
        {
            var message = await messagesService.GetMessageForSmsAsync(messageId, languageId, objectWithParameters);

            return await smsSender.SendSmsAsync(phoneTo, message);
        }

        private static Task<SendersResult> SendSmsAsync(
            this ISmsSender smsSender,
            string phoneTo,
            MessageSms message)
        {
            return smsSender.SendSmsAsync(phoneTo, message.Text);
        }
    }
}