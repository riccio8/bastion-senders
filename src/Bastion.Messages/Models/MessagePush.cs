﻿namespace Bastion.Messages.Models
{
    /// <summary>
    /// Message push.
    /// </summary>
    public class MessagePush
    {
        /// <summary>
        /// Title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Subject.
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Text.
        /// </summary>
        public string Text { get; set; }
    }
}