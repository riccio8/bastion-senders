﻿namespace Bastion.Messages.Models
{
    /// <summary>
    /// Message sms.
    /// </summary>
    public class MessageSms
    {
        /// <summary>
        /// Text.
        /// </summary>
        public string Text { get; set; }
    }
}