﻿namespace Bastion.Messages.Models
{
    /// <summary>
    /// Message email.
    /// </summary>
    public class MessageEmail
    {
        /// <summary>
        /// Html.
        /// </summary>
        public string Html { get; set; }

        /// <summary>
        /// Subject.
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Text.
        /// </summary>
        public string Text { get; set; }
    }
}