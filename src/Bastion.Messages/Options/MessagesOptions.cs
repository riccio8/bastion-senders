﻿using System.Collections.Generic;

namespace Bastion.Messages.Options
{
    /// <summary>
    /// Messages options.
    /// </summary>
    public class MessagesOptions
    {
        /// <summary>
        /// Default template id.
        /// </summary>
        public string DefaultTemplateId { get; set; } = "DEFAULT";

        /// <summary>
        /// Default Language id.
        /// </summary>
        public string DefaultLanguageId { get; set; } = "EN";

        /// <summary>
        /// Find replace values.
        /// </summary>
        public IDictionary<string, string> FindReplaceValues { get; set; } = new Dictionary<string, string>();
    }
}