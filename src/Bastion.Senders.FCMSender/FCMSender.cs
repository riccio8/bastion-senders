﻿using Bastion.Senders.Abstractions;
using Bastion.Senders.FCMSender.Models;
using Bastion.Senders.FCMSender.Options;
using Bastion.Senders.PushSender.Abstractions;
using Bastion.Senders.PushSender.Messages;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using System;
using System.Net.Http;
using System.Net.Mime;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Bastion.Senders.FCMSender
{
    /// <summary>
    /// Firebase cloud messaging push-notification sender.
    /// </summary>
    public class FCMSender : PushSenderBase, IPushSender
    {
        private static readonly JsonSerializerOptions options = new JsonSerializerOptions()
        {
            PropertyNameCaseInsensitive = true,
            IgnoreNullValues = true,
        };

        internal static Uri BaseAddress { get; } = new Uri("https://fcm.googleapis.com");

        private readonly HttpClient httpClient;
        private readonly FCMOptions optionsFCM;

        /// <summary>
        /// Initializes a new instance of <see cref="FCMSender"/>.
        /// </summary>
        public FCMSender(
            HttpClient httpClient,
            ILogger<IPushSender> logger,
            IOptions<FCMOptions> optionsFCM)
            : base(logger, optionsFCM)
        {
            this.optionsFCM = optionsFCM.Value;

            if (optionsPushSender.Enabled)
            {
                if (string.IsNullOrEmpty(optionsFCM.Value?.Key))
                {
                    logger.LogWarning("FCM, send push-notification disabled, 'Key' in configuration not found.");
                    optionsPushSender.Enabled = false;
                }
                else
                {
                    this.httpClient = httpClient;
                    this.httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", $"key={optionsFCM.Value.Key}");
                }
            }
            else
            {
                logger.LogWarning("FCM, send push-notification disabled from configuration.");
            }
        }

        /// <summary>
        /// Send push-notification.
        /// </summary>
        /// <param name="toDevice">This parameter specifies the recipient of a message.</param>
        /// <param name="title">Title.</param>
        /// <param name="subject">Subject.</param>
        /// <param name="textMessage">Plain text message.</param>
        /// <param name="channelId">channel id.</param>
        public Task<SendersResult> SendPushNotificationAsync(
            string toDevice,
            string title,
            string subject,
            string textMessage,
            string channelId = default)
        {
            return SendPushNotificationAsync(toDevice, title, subject, new PushNotificationMessageData
            {
                Message = textMessage,
            }, channelId);
        }

        /// <summary>
        /// Send data notification.
        /// </summary>
        /// <param name="toDevice">This parameter specifies the recipient of a message.</param>
        /// <param name="title">Title.</param>
        /// <param name="subject">Subject.</param>
        /// <param name="data">Data.</param>
        /// <param name="channelId">channel id.</param>
        public async Task<SendersResult> SendPushNotificationAsync<TFCMNotificationData>(
            string toDevice,
            string title,
            string subject,
            TFCMNotificationData data,
            string channelId = default)
            where TFCMNotificationData : PushNotificationDataBase
        {
            if (optionsPushSender.Enabled)
            {
                logger.LogInformation($"FCM, push-notification sending title: {title}, subject: {subject}.");

                var request = new HttpRequestMessage(HttpMethod.Post, "/fcm/send")
                {
                    Content = new StringContent(JsonSerializer.Serialize(new FCMMessage<TFCMNotificationData>
                    {
                        Data = data,
                        ToDevice = toDevice,
                        Notification = new FCMNotification
                        {
                            Title = title,
                            Subject = subject,
                            AndroidChannelId = channelId ?? optionsFCM.AndroidChannelId,
                        },
                    }, options), Encoding.UTF8, MediaTypeNames.Application.Json)
                };
                try
                {
                    var response = await httpClient.SendAsync(request);
                    if (response.IsSuccessStatusCode)
                    {
                        logger.LogInformation($"FCM, push-notification send to: {toDevice}, title: {title}");
                        return SendersResult.Success;
                    }
                    else
                    {
                        var failedMessage = $"FCM, Error when push-notification sending to: {toDevice}, title: {title}, code: {response.StatusCode}";
                        logger.LogWarning(failedMessage);
                        return SendersResult.Failed(failedMessage);
                    }
                }
                catch (Exception ex)
                {
                    var failedMessage = $"FCM, Error when push-notification sending to: {toDevice}, title: {title}, Exception: {ex.Message}";
                    logger.LogError(failedMessage);
                    return SendersResult.Failed(failedMessage);
                }
            }

            logger.LogInformation($"PushSender, Skip push-notification sending title: {title}, subject: {subject}.");

            return await Task.FromResult(SendersResult.Skip);
        }
    }
}