﻿using Bastion.Senders.PushSender.Abstractions.Options;

namespace Bastion.Senders.FCMSender.Options
{
    /// <summary>
    /// FCM options.
    /// </summary>
    public class FCMOptions : PushSenderOptions
    {
        /// <summary>
        /// Authentication key.
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Android channel id.
        /// </summary>
        public string AndroidChannelId { get; set; }
    }
}