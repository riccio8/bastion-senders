﻿using System.Text.Json.Serialization;

namespace Bastion.Senders.FCMSender.Models
{
    /// <summary>
    /// FCM notification.
    /// </summary>
    public sealed class FCMNotification
    {
        /// <summary>
        /// Title.
        /// </summary>
        [JsonPropertyName("title")]
        public string Title { get; set; }

        /// <summary>
        /// Body.
        /// </summary>
        [JsonPropertyName("body")]
        public string Subject { get; set; }

        /// <summary>
        /// Sound.
        /// </summary>
        [JsonPropertyName("sound")]
        public string Sound { get; set; } = "default";

        /// <summary>
        /// Android channel.
        /// </summary>
        [JsonPropertyName("android_channel_id")]
        public string AndroidChannelId { get; set; }
    }
}