﻿using Bastion.Senders.PushSender.Messages;

using System.Text.Json.Serialization;

namespace Bastion.Senders.FCMSender.Models
{
    /// <summary>
    /// FCM message.
    /// </summary>
    public sealed class FCMMessage<TData>
        where TData : PushNotificationDataBase
    {
        /// <summary>
        /// To device.
        /// </summary>
        [JsonPropertyName("to")]
        public string ToDevice { get; set; }

        /// <summary>
        /// Content available.
        /// </summary>
        [JsonPropertyName("content_available")]
        public bool ContentAvailable { get; set; } = true;

        /// <summary>
        /// Sound.
        /// </summary>
        [JsonPropertyName("priority")]
        public string Priority { get; set; } = "high";

        /// <summary>
        /// Notification.
        /// </summary>
        [JsonPropertyName("notification")]
        public FCMNotification Notification { get; set; }

        /// <summary>
        /// Data.
        /// </summary>
        [JsonPropertyName("data")]
        public TData Data { get; set; }
    }
}