﻿using Bastion.Senders.FCMSender;
using Bastion.Senders.FCMSender.Options;
using Bastion.Senders.PushSender.Abstractions;

using Microsoft.Extensions.Configuration;

using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Contains extension methods to <see cref="IServiceCollection"/> for configuring fcm push-notification sender.
    /// </summary>
    public static class FCMSenderServiceCollectionExtensions
    {
        /// <summary>
        /// Adds fcm push-notification sender.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/> to add services to.</param>
        /// <param name="setupAction">An action to configure the <see cref="TwilioOptions"/>.</param>
        /// <returns>The <see cref="IServiceCollection"/> so that additional calls can be chained.</returns>
        public static IServiceCollection AddFCMPushSender(this IServiceCollection services, Action<FCMOptions> setupAction)
        {
            services.AddFCMPushSender();

            if (setupAction != null)
            {
                services.Configure(setupAction);
            }

            return services;
        }

        /// <summary>
        /// Adds fcm push-notification sender.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/> to add services to.</param>
        /// <param name="configuration">The service configuration.</param>
        /// <returns>The <see cref="IServiceCollection"/> so that additional calls can be chained.</returns>
        public static IServiceCollection AddFCMPushSender(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddFCMPushSender();
            services.Configure<FCMOptions>(configuration);

            return services;
        }

        /// <summary>
        /// Adds fcm push-notification sender.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/> to add services to.</param>
        /// <returns>The <see cref="IServiceCollection"/> so that additional calls can be chained.</returns>
        public static IServiceCollection AddFCMPushSender(this IServiceCollection services)
        {
            services.AddHttpClient<IPushSender, FCMSender>(client =>
            {
                client.BaseAddress = FCMSender.BaseAddress;
                client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json; UTF-8");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            }).ConfigurePrimaryHttpMessageHandler(handler => new HttpClientHandler
            {
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
            });

            return services;
        }
    }
}