﻿namespace Bastion.Senders.Abstractions
{
    /// <summary>
    /// Represents the result of an senders operation.
    /// </summary>
    public class SendersResult
    {
        private static readonly SendersResult skip = new SendersResult { Succeeded = true, Skipped = true };
        private static readonly SendersResult success = new SendersResult { Succeeded = true, Skipped = false };

        /// <summary>
        /// Returns an <see cref="SendersResult"/> indicating a successful senders operation.
        /// </summary>
        /// <returns>An <see cref="SendersResult"/> indicating a successful operation.</returns>
        public static SendersResult Success => success;

        /// <summary>
        /// Returns an <see cref="SendersResult"/> indicating a skipped senders operation.
        /// </summary>
        /// <returns>An <see cref="SendersResult"/> indicating a skipped operation.</returns>
        public static SendersResult Skip => skip;

        /// <summary>
        /// Flag indicating whether if the operation succeeded or not.
        /// </summary>
        /// <value>True if the operation succeeded, otherwise false.</value>
        public bool Succeeded { get; protected set; }

        /// <summary>
        /// Flag indicating whether if the operation skipped or not.
        /// </summary>
        /// <value>True if the operation skipped, otherwise false.</value>
        public bool Skipped { get; protected set; }

        /// <summary>
        /// Message of error.
        /// </summary>
        public string ErrorMessage { get; protected set; }

        /// <summary>
        /// Creates a <see cref = "SendersResult" /> indicating a failed sender operation, with the parameter <paramref name = "message" />, if applicable.
        /// </summary>
        /// <param name="message">Message of error.</param>
        public static SendersResult Failed(string message)
        {
            return new SendersResult
            {
                Skipped = false,
                Succeeded = false,
                ErrorMessage = message,
            };
        }

        /// <summary>
        /// Converts the value of the current <see cref="SendersResult"/> object to its equivalent string representation.
        /// </summary>
        /// <returns>A string representation of the current <see cref="SendersResult"/> object.</returns>
        public override string ToString()
        {
            return Skipped ? "Skipped" : Succeeded ? "Succeeded" : $"Failed: {ErrorMessage}";
        }
    }
}