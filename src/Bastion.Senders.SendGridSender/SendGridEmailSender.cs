﻿using Bastion.Senders.Abstractions;
using Bastion.Senders.EmailSender.Abstractions;
using Bastion.Senders.SendGridSender.Extensions;
using Bastion.Senders.SendGridSender.Options;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using SendGrid;
using SendGrid.Helpers.Mail;

using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Bastion.Senders.SendGridSender
{
    /// <summary>
    /// SendGrid email sender.
    /// </summary>
    public class SendGridEmailSender : EmailSenderBase
    {
        private readonly bool isTracking;
        private readonly EmailAddress fromEmailAddress;
        private readonly ISendGridClient clientSendGrid;

        /// <summary>
        /// Initializes a new instance of <see cref="SendGridEmailSender"/>.
        /// </summary>
        public SendGridEmailSender(
            HttpClient httpClient,
            ILogger<IEmailSender> logger,
            IOptions<SendGridOptions> optionsSendGrid)
            : base(logger, optionsSendGrid)
        {
            if (optionsEmailSender.Enabled)
            {
                if (string.IsNullOrEmpty(optionsSendGrid.Value.Key))
                {
                    logger.LogWarning("SendGrid, send email disabled, 'Key' in configuration not found.");
                    optionsEmailSender.Enabled = false;
                }
                else
                {
                    if (string.IsNullOrEmpty(optionsEmailSender.FromEmail))
                    {
                        logger.LogWarning("SendGrid, send email disabled, 'FromEmail' in configuration not found.");
                        optionsEmailSender.Enabled = false;
                    }
                    else
                    {
                        isTracking = optionsSendGrid.Value.Tracking;
                        fromEmailAddress = new EmailAddress(optionsEmailSender.FromEmail, optionsEmailSender.FromName);
                        try
                        {
                            clientSendGrid = new SendGridClient(httpClient, optionsSendGrid.Value.Key);
                        }
                        catch (Exception ex)
                        {
                            logger.LogWarning($"SendGrid, initialize error: {ex.Message}");
                            optionsEmailSender.Enabled = false;
                        }
                    }
                }
            }
            else
            {
                logger.LogWarning("SendGrid, send email disabled from configuration.");
            }
        }

        /// <summary>
        /// Send Email.
        /// </summary>
        /// <param name="emailTo">To email.</param>
        /// <param name="subject">Subject.</param>
        /// <param name="htmlMessage">Html message.</param>
        /// <param name="textMessage">Plain text message.</param>
        public override async Task<SendersResult> SendEmailAsync(string emailTo, string subject, string htmlMessage, string textMessage)
        {
            if (optionsEmailSender.Enabled)
            {
                logger.LogInformation($"SendGrid, Email sending to: {emailTo}, subject: {subject}");

                var message = MailHelper.CreateSingleEmail(
                    fromEmailAddress,
                    new EmailAddress(emailTo),
                    subject,
                    textMessage,
                    htmlMessage);

                message.SetClickTracking(isTracking, false);

                try
                {
                    var response = await clientSendGrid.SendEmailAsync(message);
                    if (response.StatusCode.IsSuccessStatusCode())
                    {
                        logger.LogInformation($"SendGrid, Email send to: {emailTo}, subject: {subject}");
                        return SendersResult.Success;
                    }
                    else
                    {
                        var failedMessage = $"SendGrid, Error when Email sending to: {emailTo}, subject: {subject}, code: {response.StatusCode}";
                        logger.LogWarning(failedMessage);
                        return SendersResult.Failed(failedMessage);
                    }
                }
                catch (Exception ex)
                {
                    var failedMessage = $"SendGrid, Error when Email sending to: {emailTo}, subject: {subject}, Exception: {ex.Message}";
                    logger.LogWarning(failedMessage);
                    return SendersResult.Failed(failedMessage);
                }
            }
            return await base.SendEmailAsync(emailTo, subject, htmlMessage, textMessage);
        }
    }
}