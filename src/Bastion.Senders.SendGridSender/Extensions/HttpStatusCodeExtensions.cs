﻿using System.Net;

namespace Bastion.Senders.SendGridSender.Extensions
{
    internal static class HttpStatusCodeExtensions
    {
        public static bool IsSuccessStatusCode(this HttpStatusCode statusCode)
        {
            var statusCodeAsInt = (int)statusCode;
            return statusCodeAsInt >= 200 && statusCodeAsInt <= 299;
        }
    }
}