﻿using Bastion.Senders.EmailSender.Abstractions;
using Bastion.Senders.SendGridSender;
using Bastion.Senders.SendGridSender.Options;

using Microsoft.Extensions.Configuration;

using System;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Contains extension methods to <see cref="IServiceCollection"/> for configuring SendGrid email sender.
    /// </summary>
    public static class SendGridEmailSenderServiceCollectionExtensions
    {
        /// <summary>
        /// Adds SendGrid email sender.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/> to add services to.</param>
        /// <param name="setupAction">An action to configure the <see cref="SendGridOptions"/>.</param>
        /// <returns>The <see cref="IServiceCollection"/> so that additional calls can be chained.</returns>
        public static IServiceCollection AddSendGridEmailSender(this IServiceCollection services, Action<SendGridOptions> setupAction)
        {
            services.AddSendGridEmailSender();

            if (setupAction != null)
            {
                services.Configure(setupAction);
            }
            return services;
        }

        /// <summary>
        /// Adds SendGrid email sender.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/> to add services to.</param>
        /// <param name="configuration">The service configuration.</param>
        /// <returns>The <see cref="IServiceCollection"/> so that additional calls can be chained.</returns>
        public static IServiceCollection AddSendGridEmailSender(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSendGridEmailSender();

            services.Configure<SendGridOptions>(configuration);

            return services;
        }

        /// <summary>
        /// Adds SendGrid email sender.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/> to add services to.</param>
        /// <returns>The <see cref="IServiceCollection"/> so that additional calls can be chained.</returns>
        public static IServiceCollection AddSendGridEmailSender(this IServiceCollection services)
        {
            services.AddHttpClient<IEmailSender, SendGridEmailSender>();

            return services;
        }
    }
}