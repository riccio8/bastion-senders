﻿using Bastion.Senders.EmailSender.Abstractions.Options;

namespace Bastion.Senders.SendGridSender.Options
{
    /// <summary>
    /// SendGrid options.
    /// </summary>
    public class SendGridOptions : EmailSenderOptions
    {
        /// <summary>
        /// Gets or sets key.
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets tracking.
        /// </summary>
        public bool Tracking { get; set; } = false;
    }
}