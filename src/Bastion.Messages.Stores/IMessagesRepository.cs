﻿using Bastion.Messages.Stores.Models;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bastion.Messages.Stores
{
    /// <summary>
    /// Interface messages repository.
    /// </summary>
    public interface IMessagesRepository
    {
        /// <summary>
        /// Get find and replace values.
        /// </summary>
        /// <returns>The task list of find and replace values.</returns>
        Task<List<FindReplace>> GetFindReplaceAsync();

        /// <summary>
        /// Get find and replace by find.
        /// </summary>
        /// <param name="find">Find.</param>
        /// <returns>The task of find and replace value.</returns>
        Task<FindReplace> GetFindReplaceByFindAsync(string find);

        /// <summary>
        /// Delete find replace.
        /// </summary>
        /// <param name="findReplace">Find and replace value.</param>
        /// <returns>The task find and replace value.</returns>
        Task<FindReplace> DeleteFindReplaceAsync(FindReplace findReplace);

        /// <summary>
        /// Update find replace.
        /// </summary>
        /// <param name="findReplace">Find and replace value.</param>
        /// <returns>The task find and replace value.</returns>
        Task<FindReplace> UpdateFindReplaceAsync(FindReplace findReplace);

        /// <summary>
        /// Add find replace.
        /// </summary>
        /// <param name="findReplace">Find and replace value.</param>
        /// <returns>The task find and replace value.</returns>
        Task<FindReplace> AddFindReplaceAsync(FindReplace findReplace);

        /// <summary>
        /// Get templates.
        /// </summary>
        /// <returns>The task list of templates.</returns>
        Task<List<Template>> GetTemplatesAsync();

        /// <summary>
        /// Get Template by id and language.
        /// </summary>
        /// <param name="templateId">Template id.</param>
        /// <param name="languageId">Language id.</param>
        /// <returns>The task template.</returns>
        Task<Template> GetTemplateByIdAsync(string templateId, string languageId);

        /// <summary>
        /// Delete template.
        /// </summary>
        /// <param name="template">Template.</param>
        /// <returns>The task template.</returns>
        Task<Template> DeleteTemplateAsync(Template template);

        /// <summary>
        /// Update template.
        /// </summary>
        /// <param name="template">Template.</param>
        /// <returns>The task template.</returns>
        Task<Template> UpdateTemplateAsync(Template template);

        /// <summary>
        /// Add template.
        /// </summary>
        /// <param name="template">Template.</param>
        /// <returns>The task template.</returns>
        Task<Template> AddTemplateAsync(Template template);

        /// <summary>
        /// Get messages.
        /// </summary>
        /// <returns>The task list or message.</returns>
        Task<List<Message>> GetMessagesAsync();

        /// <summary>
        /// Get messages paginated.
        /// </summary>
        /// <param name="search">Ssearch.</param>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="pageSize">Page size.</param>
        Task<PaginatedList<Message>> GetMessagesPaginatedAsync(string search, int pageIndex, int pageSize);

        /// <summary>
        /// Get message by id and language.
        /// </summary>
        /// <param name="messageId">Message id.</param>
        /// <param name="languageId">Language id.</param>
        /// <returns>The task message.</returns>
        Task<Message> GetMessageByIdAsync(string messageId, string languageId);

        /// <summary>
        /// Get message by group id.
        /// </summary>
        /// <param name="groupId">Group id.</param>
        /// <param name="pageIndex">Group id.</param>
        /// <param name="pageSize">Group id.</param>
        /// <returns>The task message.</returns>
        Task<PaginatedList<Message>> GetMessagesByGroupPaginatedAsync(string groupId, int pageIndex, int pageSize);

        /// <summary>
        /// Get message by group id.
        /// </summary>
        /// <param name="groupId">Group id.</param>
        /// <returns>The task message.</returns>
        Task<List<Message>> GetMessagesByGroupAsync(string groupId);

        /// <summary>
        /// Delete message.
        /// </summary>
        /// <param name="message">Message.</param>
        /// <returns>The task message.</returns>
        Task<Message> DeleteMessageAsync(Message message);

        /// <summary>
        /// Update message.
        /// </summary>
        /// <param name="message">Message.</param>
        /// <returns>The task message.</returns>
        Task<Message> UpdateMessageAsync(Message message);

        /// <summary>
        /// Add message.
        /// </summary>
        /// <param name="message">Message.</param>
        /// <returns>The task message.</returns>
        Task<Message> AddMessageAsync(Message message);

        /// <summary>
        /// Get message group.
        /// </summary>
        /// <returns>The task message group.</returns>
        Task<PaginatedList<MessageGroup>> GetMessageGroupPaginatedAsync(int pageIndex, int pageSize);

        /// <summary>
        /// Get message group.
        /// </summary>
        /// <param name="groupId">Group id.</param>
        /// <returns>The task message group.</returns>
        Task<MessageGroup> GetMessageGroupByIdAsync(string groupId);

        /// <summary>
        /// Delete message group.
        /// </summary>
        /// <param name="messageGroup">Message group.</param>
        /// <returns>The task message group.</returns>
        Task<MessageGroup> DeleteMessageGroupAsync(MessageGroup messageGroup);

        /// <summary>
        /// Update message group.
        /// </summary>
        /// <param name="messageGroup">Message group.</param>
        /// <returns>The task message group.</returns>
        Task<MessageGroup> UpdateMessageGroupAsync(MessageGroup messageGroup);

        /// <summary>
        /// Update message type.
        /// </summary>
        /// <param name="messageType">Message type.</param>
        /// <returns>The task message type.</returns>
        Task<MessageType> UpdateMessageTypeAsync(MessageType messageType);

        /// <summary>
        /// Get message type.
        /// </summary>
        /// <param name="messageId">Message id.</param>
        /// <returns>The task message type.</returns>
        Task<MessageType> GetMessageTypeByIdAsync(string messageId);

        /// <summary>
        /// Add message group.
        /// </summary>
        /// <param name="messageGroup">Message group.</param>
        /// <returns>The task message group.</returns>
        Task<MessageGroup> AddMessageGroupAsync(MessageGroup messageGroup);
    }
}