﻿using System;
using System.Collections.Generic;

namespace Bastion.Messages.Stores.Models
{
    /// <summary>
    /// Template.
    /// </summary>
    public partial class Template
    {
        /// <summary>
        /// Header.
        /// </summary>
        public string Header { get; set; }

        /// <summary>
        /// Footer.
        /// </summary>
        public string Footer { get; set; }

        /// <summary>
        /// Template id.
        /// </summary>
        public string TemplateId { get; set; }

        /// <summary>
        /// Language id.
        /// </summary>
        public string LanguageId { get; set; }

        /// <summary>
        /// Modification at.
        /// </summary>
        public DateTime ModificationAt { get; set; }

        /// <summary>
        /// Messages.
        /// </summary>
        public virtual ICollection<Message> Messages { get; set; } = new HashSet<Message>();
    }
}