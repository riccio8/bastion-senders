﻿using System.Collections.Generic;

namespace Bastion.Messages.Stores.Models
{
    public partial class MessageType
    {
        /// <summary>
        /// Message id.
        /// </summary>
        public string MessageId { get; set; }

        /// <summary>
        /// Message group id.
        /// </summary>
        public string MessageGroupId { get; set; }

        /// <summary>
        /// Description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Message group.
        /// </summary>
        public virtual MessageGroup MessageGroup { get; set; }

        /// <summary>
        /// Messages.
        /// </summary>
        public virtual ICollection<Message> Messages { get; set; } = new HashSet<Message>();
    }
}