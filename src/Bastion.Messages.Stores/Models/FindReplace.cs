﻿using System;

namespace Bastion.Messages.Stores.Models
{
    /// <summary>
    /// Find and replace value.
    /// </summary>
    public partial class FindReplace
    {
        /// <summary>
        /// Find text.
        /// </summary>
        public string Find { get; set; }

        /// <summary>
        /// Replace text.
        /// </summary>
        public string Replace { get; set; }

        /// <summary>
        /// Modification at.
        /// </summary>
        public DateTime ModificationAt { get; set; }
    }
}