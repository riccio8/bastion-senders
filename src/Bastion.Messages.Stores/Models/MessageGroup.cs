﻿using System.Collections.Generic;

namespace Bastion.Messages.Stores.Models
{
    /// <summary>
    /// Message group.
    /// </summary>
    public partial class MessageGroup
    {
        /// <summary>
        /// Message id.
        /// </summary>
        public string MessageGroupId { get; set; }

        /// <summary>
        /// Description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Message type.
        /// </summary>
        public virtual ICollection<MessageType> MessageTypes { get; set; } = new HashSet<MessageType>();
    }
}