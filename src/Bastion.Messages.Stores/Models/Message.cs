﻿using System;

namespace Bastion.Messages.Stores.Models
{
    /// <summary>
    /// Message.
    /// </summary>
    public partial class Message
    {
        /// <summary>
        /// Message id.
        /// </summary>
        public string MessageId { get; set; }

        /// <summary>
        /// Language id.
        /// </summary>
        public string LanguageId { get; set; }

        /// <summary>
        /// Template id.
        /// </summary>
        public string TemplateId { get; set; }

        /// <summary>
        /// PreView.
        /// </summary>
        public string PreView { get; set; }

        /// <summary>
        /// Subject.
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Context html.
        /// </summary>
        public string ContextHtml { get; set; }

        /// <summary>
        /// Context text.
        /// </summary>
        public string ContextText { get; set; }

        /// <summary>
        /// Context sms.
        /// </summary>
        public string ContextSms { get; set; }

        /// <summary>
        /// Modification at.
        /// </summary>
        public DateTime ModificationAt { get; set; }

        /// <summary>
        /// Description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Message type.
        /// </summary>
        public virtual MessageType MessageType { get; set; }

        /// <summary>
        /// Template.
        /// </summary>
        public virtual Template Template { get; set; }
    }
}