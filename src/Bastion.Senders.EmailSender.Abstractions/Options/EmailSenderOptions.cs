﻿namespace Bastion.Senders.EmailSender.Abstractions.Options
{
    /// <summary>
    /// EMail sender Options.
    /// </summary>
    public class EmailSenderOptions
    {
        /// <summary>
        /// Gets or sets Enabled.
        /// </summary>
        public bool Enabled { get; set; } = false;

        /// <summary>
        /// Gets or sets from email.
        /// </summary>
        public string FromEmail { get; set; }

        /// <summary>
        /// Gets or sets from name.
        /// </summary>
        public string FromName { get; set; }
    }
}