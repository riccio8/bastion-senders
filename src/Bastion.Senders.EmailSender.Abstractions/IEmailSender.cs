﻿using Bastion.Senders.Abstractions;

using System.Threading.Tasks;

namespace Bastion.Senders.EmailSender.Abstractions
{
    /// <summary>
    /// Interface email sender.
    /// </summary>
    public interface IEmailSender
    {
        /// <summary>
        /// Send Email.
        /// </summary>
        /// <param name="emailTo">To email.</param>
        /// <param name="subject">Subject.</param>
        /// <param name="htmlMessage">Html message.</param>
        /// <param name="textMessage">Plain text message.</param>
        Task<SendersResult> SendEmailAsync(string emailTo, string subject, string htmlMessage, string textMessage);
    }
}