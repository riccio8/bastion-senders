﻿using Bastion.Senders.Abstractions;
using Bastion.Senders.EmailSender.Abstractions.Options;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using System.Threading.Tasks;

namespace Bastion.Senders.EmailSender.Abstractions
{
    /// <summary>
    /// Email Sender base.
    /// </summary>
    public abstract class EmailSenderBase : IEmailSender
    {
        /// <summary>
        /// Logger.
        /// </summary>
        protected readonly ILogger<IEmailSender> logger;

        /// <summary>
        /// Options email sender.
        /// </summary>
        protected readonly EmailSenderOptions optionsEmailSender;

        /// <summary>
        /// Initializes a new instance of <see cref="EmailSenderBase"/>.
        /// </summary>
        protected EmailSenderBase(
            ILogger<IEmailSender> logger,
            IOptions<EmailSenderOptions> optionsEmailSender)
        {
            this.logger = logger;
            this.optionsEmailSender = optionsEmailSender.Value;
        }

        /// <summary>
        /// Send Email.
        /// </summary>
        /// <param name="emailTo">To email.</param>
        /// <param name="subject">Subject.</param>
        /// <param name="htmlMessage">Html message.</param>
        /// <param name="textMessage">Plain text message.</param>
        public virtual Task<SendersResult> SendEmailAsync(string emailTo, string subject, string htmlMessage, string textMessage)
        {
            logger.LogInformation($"EmailSender, Skip email sending to: {emailTo}, subject: {subject}");

            return Task.FromResult(SendersResult.Skip);
        }
    }
}