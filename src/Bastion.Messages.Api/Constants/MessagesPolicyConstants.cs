﻿namespace Bastion.Messages.Api.Constants
{
    /// <summary>
    /// Messages policy constants.
    /// </summary>
    public static class MessagesPolicyConstants
    {
        /// <summary>
        /// Management policy.
        /// </summary>
        public const string ManagementPolicy = "MessagesManagementPolicy";
    }
}