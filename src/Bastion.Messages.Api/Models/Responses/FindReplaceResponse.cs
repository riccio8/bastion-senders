﻿using Bastion.Messages.Stores.Models;

using System;

namespace Bastion.Messages.Api.Models.Responses
{
    /// <summary>
    /// Find replace response.
    /// </summary>
    public class FindReplaceResponse
    {
        /// <summary>
        /// Implicit find replace to response conversion.
        /// </summary>
        /// <param name="findReplace">FindReplace.</param>
        public static implicit operator FindReplaceResponse(FindReplace findReplace)
        {
            return new FindReplaceResponse
            {
                Find = findReplace.Find,
                Replace = findReplace.Replace,
                ModificationAt = findReplace.ModificationAt,
            };
        }

        /// <summary>
        /// Find.
        /// </summary>
        public string Find { get; set; }

        /// <summary>
        /// Replace.
        /// </summary>
        public string Replace { get; set; }

        /// <summary>
        /// Modification at.
        /// </summary>
        public DateTime ModificationAt { get; set; }
    }
}