﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace Bastion.Messages.Api.Models.Responses
{
    /// <summary>
    /// Page response base.
    /// </summary>
    [DataContract]
    public abstract class PageResponseBase<T> where T : class
    {
        /// <summary>
        /// Page.
        /// </summary>
        [JsonPropertyName("page")]
        [DataMember(Name = "page")]
        public int Page { get; set; }

        /// <summary>
        /// Size of page.
        /// </summary>
        [JsonPropertyName("perPage")]
        [DataMember(Name = "perPage")]
        public int Size { get; set; }

        /// <summary>
        /// Count pages.
        /// </summary>
        [JsonPropertyName("pages")]
        [DataMember(Name = "pages")]
        public int CountPages { get; set; }

        /// <summary>
        /// Count.
        /// </summary>
        [JsonPropertyName("count")]
        [DataMember(Name = "count")]
        public int Count { get; set; }

        /// <summary>
        /// Data.
        /// </summary>
        [JsonPropertyName("data")]
        [DataMember(Name = "data")]
        public IEnumerable<T> Data { get; set; }
    }
}