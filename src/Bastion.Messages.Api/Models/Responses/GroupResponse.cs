﻿using Bastion.Messages.Stores.Models;

namespace Bastion.Messages.Api.Models.Responses
{
    /// <summary>
    /// Message group response.
    /// </summary>
    public class GroupResponse
    {
        /// <summary>
        /// Implicit message group to response conversion.
        /// </summary>
        /// <param name="group">Message group.</param>
        public static explicit operator GroupResponse(MessageGroup group)
        {
            return new GroupResponse
            {
                Description = group.Description,
                GroupId = group.MessageGroupId,
            };
        }

        /// <summary>
        /// Description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Group id.
        /// </summary>
        public string GroupId { get; set; }
    }
}