﻿using Bastion.Messages.Stores.Models;

namespace Bastion.Messages.Api.Models.Responses
{
    /// <summary>
    /// Message type response.
    /// </summary>
    public class MessageTypeResponse
    {
        /// <summary>
        /// Implicit message group to response conversion.
        /// </summary>
        /// <param name="type">Message type.</param>
        public static explicit operator MessageTypeResponse(MessageType type)
        {
            return new MessageTypeResponse
            {
                Description = type.Description,
                MessageId = type.MessageId,
                MessageGroupId = type.MessageGroupId
            };
        }

        /// <summary>
        /// Description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Message id.
        /// </summary>
        public string MessageId { get; set; }

        /// <summary>
        /// Message group id.
        /// </summary>
        public string MessageGroupId { get; set; }
    }
}