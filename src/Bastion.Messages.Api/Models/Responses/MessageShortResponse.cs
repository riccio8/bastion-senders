﻿using Bastion.Messages.Stores.Models;

using System;

namespace Bastion.Messages.Api.Models.Responses
{
    /// <summary>
    /// Message response.
    /// </summary>
    public class MessageShortResponse
    {
        /// <summary>
        /// Implicit message to response conversion.
        /// </summary>
        /// <param name="message">Message.</param>
        public static implicit operator MessageShortResponse(Message message)
        {
            return new MessageShortResponse
            {
                Subject = message.Subject,
                MessageId = message.MessageId,
                LanguageId = message.LanguageId,
                TemplateId = message.TemplateId,
                ModificationAt = message.ModificationAt,
                Description = message.Description,
            };
        }

        /// <summary>
        /// Subject.
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Message id.
        /// </summary>
        public string MessageId { get; set; }

        /// <summary>
        /// Language id.
        /// </summary>
        public string LanguageId { get; set; }

        /// <summary>
        /// Template id.
        /// </summary>
        public string TemplateId { get; set; }

        /// <summary>
        /// Modification at.
        /// </summary>
        public DateTime ModificationAt { get; set; }

        /// <summary>
        /// Description.
        /// </summary>
        public string Description { get; set; }
    }
}