﻿using Bastion.Messages.Stores.Models;

using System.Linq;
using System.Runtime.Serialization;

namespace Bastion.Messages.Api.Models.Responses
{
    /// <summary>
    /// Groups page response.
    /// </summary>
    [DataContract]
    public class GroupsPageResponse : PageResponseBase<GroupResponse>
    {
        /// <summary>
        /// Implicit groups to page response conversion.
        /// </summary>
        /// <param name="groups">Paginated list of groups.</param>
        public static explicit operator GroupsPageResponse(PaginatedList<MessageGroup> groups)
        {
            return new GroupsPageResponse
            {
                Size = groups.PageSize,
                Page = groups.PageIndex + 1,
                Count = groups.CountTotal,
                CountPages = groups.PageCount,
                Data = groups.Select(x => (GroupResponse)x),
            };
        }
    }
}