﻿using Bastion.Messages.Stores.Models;

namespace Bastion.Messages.Api.Models.Responses
{
    /// <summary>
    /// Message response.
    /// </summary>
    public class MessageResponse : MessageShortResponse
    {
        /// <summary>
        /// Implicit message to response conversion.
        /// </summary>
        /// <param name="message">Message.</param>
        public static implicit operator MessageResponse(Message message)
        {
            return new MessageResponse
            {
                PreView = message.PreView,
                Subject = message.Subject,
                MessageId = message.MessageId,
                LanguageId = message.LanguageId,
                TemplateId = message.TemplateId,
                ContextSms = message.ContextSms,
                ContextHtml = message.ContextHtml,
                ContextText = message.ContextText,
                ModificationAt = message.ModificationAt,
            };
        }

        /// <summary>
        /// Get or set preView.
        /// </summary>
        public string PreView { get; set; }

        /// <summary>
        /// Get or set context sms.
        /// </summary>
        public string ContextSms { get; set; }

        /// <summary>
        /// Context html.
        /// </summary>
        public string ContextHtml { get; set; }

        /// <summary>
        /// Context text.
        /// </summary>
        public string ContextText { get; set; }
    }
}