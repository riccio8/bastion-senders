﻿using Bastion.Messages.Stores.Models;

using System;

namespace Bastion.Messages.Api.Models.Responses
{
    /// <summary>
    /// Template response.
    /// </summary>
    public class TemplateResponse
    {
        /// <summary>
        /// Implicit template to response conversion.
        /// </summary>
        /// <param name="template">Template.</param>
        public static implicit operator TemplateResponse(Template template)
        {
            return new TemplateResponse
            {
                Header = template.Header,
                Footer = template.Footer,
                LanguageId = template.LanguageId,
                TemplateId = template.TemplateId,
                ModificationAt = template.ModificationAt,
            };
        }

        /// <summary>
        /// Header.
        /// </summary>
        public string Header { get; set; }

        /// <summary>
        /// Footer.
        /// </summary>
        public string Footer { get; set; }

        /// <summary>
        /// Language id.
        /// </summary>
        public string LanguageId { get; set; }

        /// <summary>
        /// Template id.
        /// </summary>
        public string TemplateId { get; set; }

        /// <summary>
        /// Modification at.
        /// </summary>
        public DateTime ModificationAt { get; set; }
    }
}