﻿using Bastion.Messages.Stores.Models;

using System.Linq;
using System.Runtime.Serialization;

namespace Bastion.Messages.Api.Models.Responses
{
    /// <summary>
    /// Messages page response.
    /// </summary>
    [DataContract]
    public class MessagesPageResponse : PageResponseBase<MessageShortResponse>
    {
        /// <summary>
        /// Implicit messages to page response conversion.
        /// </summary>
        /// <param name="messages">Paginated list of messages.</param>
        public static explicit operator MessagesPageResponse(PaginatedList<Message> messages)
        {
            return new MessagesPageResponse
            {
                Size = messages.PageSize,
                Page = messages.PageIndex + 1,
                Count = messages.CountTotal,
                CountPages = messages.PageCount,
                Data = messages.Select(x => (MessageShortResponse)x),
            };
        }
    }
}