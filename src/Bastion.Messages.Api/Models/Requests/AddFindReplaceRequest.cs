﻿using Bastion.Messages.Stores.Models;

using System.ComponentModel.DataAnnotations;

namespace Bastion.Messages.Api.Models.Requests
{
    /// <summary>
    /// Add find replace request.
    /// </summary>
    public class AddFindReplaceRequest : UpdateFindReplaceRequest
    {
        /// <summary>
        /// Implicit request to find replace conversion.
        /// </summary>
        /// <param name="request">Add find replace request.</param>
        public static implicit operator FindReplace(AddFindReplaceRequest request)
        {
            return new FindReplace
            {
                Find = request.Find,
                Replace = request.Replace,
            };
        }

        /// <summary>
        /// Find.
        /// </summary>
        [Required]
        [StringLength(36)]
        public string Find { get; set; }
    }
}