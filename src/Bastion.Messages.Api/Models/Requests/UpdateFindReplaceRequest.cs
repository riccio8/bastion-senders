﻿namespace Bastion.Messages.Api.Models.Requests
{
    /// <summary>
    /// Update find replace request.
    /// </summary>
    public class UpdateFindReplaceRequest
    {
        /// <summary>
        /// Replace.
        /// </summary>
        public string Replace { get; set; }
    }
}