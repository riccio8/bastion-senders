﻿using Bastion.Messages.Stores.Models;

using System.ComponentModel.DataAnnotations;

namespace Bastion.Messages.Api.Models.Requests
{
    /// <summary>
    /// Update template request.
    /// </summary>
    public class AddTemplateRequest : UpdateTemplateRequest
    {
        /// <summary>
        /// Implicit request to template conversion.
        /// </summary>
        /// <param name="request">Add find replace request.</param>
        public static implicit operator Template(AddTemplateRequest request)
        {
            return new Template
            {
                Footer = request.Footer,
                Header = request.Header,
                LanguageId = request.LanguageId,
                TemplateId = request.TemplateId,
            };
        }

        /// <summary>
        /// Template id.
        /// </summary>
        [Required]
        [StringLength(36)]
        public string TemplateId { get; set; }

        /// <summary>
        /// Language id.
        /// </summary>
        [Required]
        [StringLength(3)]
        public string LanguageId { get; set; }
    }
}