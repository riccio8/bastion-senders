﻿namespace Bastion.Messages.Api.Models.Requests
{
    /// <summary>
    /// Update template request.
    /// </summary>
    public class UpdateTemplateRequest
    {
        /// <summary>
        /// Header.
        /// </summary>
        public string Header { get; set; }

        /// <summary>
        /// Footer.
        /// </summary>
        public string Footer { get; set; }
    }
}