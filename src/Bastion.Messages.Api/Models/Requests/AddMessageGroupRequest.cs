﻿using Bastion.Messages.Stores.Models;

namespace Bastion.Messages.Api.Models.Requests
{
    /// <summary>
    /// Add message group request.
    /// </summary>
    public class AddMessageGroupRequest
    {
        /// <summary>
        /// Implicit request to message conversion.
        /// </summary>
        /// <param name="request">Add message group request.</param>
        public static implicit operator MessageGroup(AddMessageGroupRequest request)
        {
            return new MessageGroup
            {
                MessageGroupId = request.MessageGroupId,
                Description = request.Description,
            };
        }

        /// <summary>
        /// Message id.
        /// </summary>
        public string MessageGroupId { get; set; }

        /// <summary>
        /// Description.
        /// </summary>
        public string Description { get; set; }
    }
}