﻿namespace Bastion.Messages.Api.Models.Requests
{
    /// <summary>
    /// Update message type request.
    /// </summary>
    public class UpdateMessageTypeRequest
    {
        /// <summary>
        /// Description.
        /// </summary>
        public string Description { get; set; }
    }
}