﻿namespace Bastion.Messages.Api.Models.Requests
{
    /// <summary>
    /// Update message group request.
    /// </summary>
    public class UpdateMessageGroupRequest
    {
        /// <summary>
        /// Description.
        /// </summary>
        public string Description { get; set; }
    }
}