﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Bastion.Messages.Api.Models.Requests
{
    /// <summary>
    /// Pagination request.
    /// </summary>
    public class PaginationRequest
    {
        /// <summary>
        /// Per page.
        /// </summary>
        [DefaultValue(10)]
        [Range(1, int.MaxValue)]
        public int PerPage { get; set; } = 10;

        /// <summary>
        /// Page.
        /// </summary>
        [DefaultValue(1)]
        [Range(1, int.MaxValue)]
        public int Page { get; set; } = 1;
    }
}