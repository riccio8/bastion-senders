﻿using Bastion.Messages.Stores.Models;

using System.ComponentModel.DataAnnotations;

namespace Bastion.Messages.Api.Models.Requests
{
    /// <summary>
    /// Add message request.
    /// </summary>
    public class AddMessageRequest : UpdateMessageRequest
    {
        /// <summary>
        /// Implicit request to message conversion.
        /// </summary>
        /// <param name="request">Add message request.</param>
        public static implicit operator Message(AddMessageRequest request)
        {
            return new Message
            {
                Subject = request.Subject,
                PreView = request.PreView,
                MessageId = request.MessageId,
                LanguageId = request.LanguageId,
                TemplateId = request.TemplateId,
                ContextSms = request.ContextSms,
                ContextText = request.ContextText,
                ContextHtml = request.ContextHtml,
            };
        }

        /// <summary>
        /// Message id.
        /// </summary>
        [Required]
        [StringLength(36)]
        public string MessageId { get; set; }

        /// <summary>
        /// Language id.
        /// </summary>
        [Required]
        [StringLength(3)]
        public string LanguageId { get; set; }
    }
}