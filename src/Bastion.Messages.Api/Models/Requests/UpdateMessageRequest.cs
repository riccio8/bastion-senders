﻿using System.ComponentModel.DataAnnotations;

namespace Bastion.Messages.Api.Models.Requests
{
    /// <summary>
    /// Update message request.
    /// </summary>
    public class UpdateMessageRequest
    {
        /// <summary>
        /// Template id.
        /// </summary>
        [StringLength(36)]
        public string TemplateId { get; set; }

        /// <summary>
        /// PreView.
        /// </summary>
        [StringLength(255)]
        public string PreView { get; set; }

        /// <summary>
        /// Subject.
        /// </summary>
        [StringLength(255)]
        public string Subject { get; set; }

        /// <summary>
        /// Context html.
        /// </summary>
        public string ContextHtml { get; set; }

        /// <summary>
        /// Context text.
        /// </summary>
        public string ContextText { get; set; }

        /// <summary>
        /// Context sms.
        /// </summary>
        [StringLength(255)]
        public string ContextSms { get; set; }

        /// <summary>
        /// Description.
        /// </summary>
        public string Description { get; set; }
    }
}