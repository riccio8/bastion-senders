﻿using Bastion.Messages.Api.Constants;
using Bastion.Messages.Api.Models.Requests;
using Bastion.Messages.Api.Models.Responses;
using Bastion.Messages.Stores;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System.Net.Mime;
using System.Threading.Tasks;

namespace Bastion.Messages.Api.Controllers
{
    /// <summary>
    /// Groups for messages controller.
    /// </summary>
    [ApiController]
    [Consumes(MediaTypeNames.Application.Json)]
    [Produces(MediaTypeNames.Application.Json)]
    [Authorize(Policy = MessagesPolicyConstants.ManagementPolicy)]
    public class GroupsController : ControllerBase
    {
        private readonly IMessagesRepository messagesRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="GroupsController"/> class.
        /// </summary>
        public GroupsController(IMessagesRepository messagesRepository)
        {
            this.messagesRepository = messagesRepository;
        }

        /// <summary>
        /// Get messages groups paginated.
        /// </summary>
        [HttpGet("/api/v1/messages/groups")]
        public async Task<GroupsPageResponse> GetMessageGroupPaginatedAsync(
            [FromQuery] PaginationRequest request)
        {
            var messagesGroups = await messagesRepository.GetMessageGroupPaginatedAsync(
                pageIndex: request.Page - 1,
                pageSize: request.PerPage);

            return (GroupsPageResponse)messagesGroups;
        }

        /// <summary>
        /// Add message group.
        /// </summary>
        /// <param name="request">Request.</param>
        [HttpPost("/api/v1/messages/groups")]
        [ProducesResponseType(typeof(GroupResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<GroupResponse>> AddMessageGroupAsync(
            AddMessageGroupRequest request)
        {
            var messageGroup = await messagesRepository.GetMessageGroupByIdAsync(request.MessageGroupId);
            if (messageGroup != null)
            {
                return NotFound();
            }

            messageGroup = await messagesRepository.AddMessageGroupAsync(request);

            return Ok((GroupResponse)messageGroup);
        }

        /// <summary>
        /// Update message group by id.
        /// </summary>
        /// <param name="groupId">Group id.</param>
        /// <param name="request">Request.</param>
        [HttpPatch("/api/v1/messages/groups/{groupId}")]
        [ProducesResponseType(typeof(GroupResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<GroupResponse>> UpdateMessageGroupByIdAsync(
            string groupId,
            UpdateMessageGroupRequest request)
        {
            var group = await messagesRepository.GetMessageGroupByIdAsync(groupId);
            if (group is null)
            {
                return NotFound();
            }

            group.Description = request.Description ?? group.Description;

            await messagesRepository.UpdateMessageGroupAsync(group);

            return Ok((GroupResponse)group);
        }

        /// <summary>
        /// Delete group by id.
        /// </summary>
        /// <param name="groupId">Message id.</param>
        [HttpDelete("/api/v1/messages/groups/{groupId}")]
        [ProducesResponseType(typeof(GroupResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<GroupResponse>> DeleteGroupAsync(
            string groupId)
        {
            var group = await messagesRepository.GetMessageGroupByIdAsync(groupId);
            if (group is null)
            {
                return NotFound();
            }

            await messagesRepository.DeleteMessageGroupAsync(group);

            return Ok((GroupResponse)group);
        }

        /// <summary>
        /// Update message type by id.
        /// </summary>
        /// <param name="messageId">Message id.</param>
        /// <param name="request">Request.</param>
        [HttpPatch("/api/v1/messages/types/{messageId}")]
        [ProducesResponseType(typeof(MessageTypeResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<MessageTypeResponse>> UpdateMessageTypeByIdAsync(
            string messageId,
            UpdateMessageTypeRequest request)
        {
            var messageType = await messagesRepository.GetMessageTypeByIdAsync(messageId);
            if (messageType is null)
            {
                return NotFound();
            }

            messageType.Description = request.Description ?? messageType.Description;

            await messagesRepository.UpdateMessageTypeAsync(messageType);

            return Ok((MessageTypeResponse)messageType);
        }
    }
}