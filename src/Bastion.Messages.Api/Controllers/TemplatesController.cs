﻿using Bastion.Messages.Api.Constants;
using Bastion.Messages.Api.Models.Requests;
using Bastion.Messages.Api.Models.Responses;
using Bastion.Messages.Stores;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace Bastion.Messages.Api.Controllers
{
    /// <summary>
    /// Templates for messages controller.
    /// </summary>
    [ApiController]
    [Consumes(MediaTypeNames.Application.Json)]
    [Produces(MediaTypeNames.Application.Json)]
    public class TemplatesController : ControllerBase
    {
        private readonly IMessagesRepository messagesRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="TemplatesController"/> class.
        /// </summary>
        public TemplatesController(IMessagesRepository messagesRepository)
        {
            this.messagesRepository = messagesRepository;
        }

        /// <summary>
        /// Get all templates.
        /// </summary>
        [HttpGet("/api/v1/messages/templates")]
        [Authorize(Policy = MessagesPolicyConstants.ManagementPolicy)]
        [ProducesResponseType(typeof(IEnumerable<TemplateResponse>), StatusCodes.Status200OK)]
        public async Task<IEnumerable<TemplateResponse>> GetTemplatesAsync()
        {
            return (await messagesRepository.GetTemplatesAsync()).Select(x => (TemplateResponse)x);
        }

        /// <summary>
        /// Get template by id.
        /// </summary>
        /// <param name="templateId">Template id.</param>
        /// <param name="languageId">Language id.</param>
        [HttpGet("/api/v1/messages/templates/{templateId}/{languageId}")]
        [Authorize(Policy = MessagesPolicyConstants.ManagementPolicy)]
        [ProducesResponseType(typeof(TemplateResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<TemplateResponse>> GetTemplateByIdAsync(
            string templateId,
            string languageId)
        {
            var template = await messagesRepository.GetTemplateByIdAsync(templateId, languageId);
            if (template is null)
            {
                return NotFound();
            }
            return Ok((TemplateResponse)template);
        }

        /// <summary>
        /// Delete template by id.
        /// </summary>
        /// <param name="templateId">Template id.</param>
        /// <param name="languageId">Language id.</param>
        [HttpDelete("/api/v1/messages/templates/{templateId}/{languageId}")]
        [Authorize(Policy = MessagesPolicyConstants.ManagementPolicy)]
        [ProducesResponseType(typeof(TemplateResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<TemplateResponse>> DeleteTemplateByIdAsync(
            string templateId,
            string languageId)
        {
            var template = await messagesRepository.GetTemplateByIdAsync(templateId, languageId);
            if (template is null)
            {
                return NotFound();
            }

            await messagesRepository.DeleteTemplateAsync(template);

            return Ok((TemplateResponse)template);
        }

        /// <summary>
        /// Update template by id.
        /// </summary>
        /// <param name="templateId">Template id.</param>
        /// <param name="languageId">Language id.</param>
        /// <param name="request">Request.</param>
        [HttpPatch("/api/v1/messages/templates/{templateId}/{languageId}")]
        [Authorize(Policy = MessagesPolicyConstants.ManagementPolicy)]
        [ProducesResponseType(typeof(TemplateResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<TemplateResponse>> UpdateTemplateByIdAsync(
            string templateId,
            string languageId,
            UpdateTemplateRequest request)
        {
            var template = await messagesRepository.GetTemplateByIdAsync(templateId, languageId);
            if (template is null)
            {
                return NotFound();
            }

            template.Header = request?.Header ?? template.Header;
            template.Footer = request?.Footer ?? template.Footer;

            await messagesRepository.UpdateTemplateAsync(template);

            return Ok((TemplateResponse)template);
        }

        /// <summary>
        /// Add template.
        /// </summary>
        /// <param name="request">Request.</param>
        [HttpPost("/api/v1/messages/templates")]
        [Authorize(Policy = MessagesPolicyConstants.ManagementPolicy)]
        [ProducesResponseType(typeof(TemplateResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<TemplateResponse>> AddFindReplaceByFindAsync(
            AddTemplateRequest request)
        {
            var template = await messagesRepository.GetTemplateByIdAsync(request.TemplateId, request.LanguageId);
            if (template != null)
            {
                return BadRequest();
            }

            template = await messagesRepository.AddTemplateAsync(request);

            return Ok((TemplateResponse)template);
        }
    }
}