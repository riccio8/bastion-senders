﻿using Bastion.Messages.Api.Constants;
using Bastion.Messages.Api.Models.Requests;
using Bastion.Messages.Api.Models.Responses;
using Bastion.Messages.Stores;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace Bastion.Messages.Api.Controllers
{
    /// <summary>
    /// Messages controller.
    /// </summary>
    [ApiController]
    [Consumes(MediaTypeNames.Application.Json)]
    [Produces(MediaTypeNames.Application.Json)]
    [Authorize(Policy = MessagesPolicyConstants.ManagementPolicy)]
    public class MessagesController : ControllerBase
    {
        private readonly IMessagesRepository messagesRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="MessagesController"/> class.
        /// </summary>
        public MessagesController(IMessagesRepository messagesRepository)
        {
            this.messagesRepository = messagesRepository;
        }

        /// <summary>
        /// Get messages.
        /// </summary>
        [HttpGet("/api/v1/messages")]
        public async Task<IEnumerable<MessageShortResponse>> GetMessagesAsync()
        {
            var messages = await messagesRepository.GetMessagesAsync();

            return messages.Select(x => (MessageShortResponse)x);
        }

        /// <summary>
        /// Get messages paginated.
        /// </summary>
        [HttpGet("/api/v2/messages")]
        public async Task<MessagesPageResponse> GetMessagesPaginatedAsync(
            string search,
            [FromQuery] PaginationRequest request)
        {
            var messages = await messagesRepository.GetMessagesPaginatedAsync(
                search: search,
                pageIndex: request.Page - 1,
                pageSize: request.PerPage);

            return (MessagesPageResponse)messages;
        }

        /// <summary>
        /// Get messages paginated.
        /// </summary>
        [HttpGet("/api/v3/messages")]
        public async Task<MessagesPageResponse> GetMessagesByGroupPaginatedAsync(
            string groupId,
            [FromQuery] PaginationRequest request)
        {
            var messages = await messagesRepository.GetMessagesByGroupPaginatedAsync(
                groupId: groupId,
                pageIndex: request.Page - 1,
                pageSize: request.PerPage);

            return (MessagesPageResponse)messages;
        }

        /// <summary>
        /// Get message by id.
        /// </summary>
        /// <param name="messageId">Message id.</param>
        /// <param name="languageId">Language id.</param>
        [HttpGet("/api/v1/messages/{messageId}/{languageId}")]
        [ProducesResponseType(typeof(MessageResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<MessageResponse>> GetMessageByIdAsync(
            string messageId,
            string languageId)
        {
            var message = await messagesRepository.GetMessageByIdAsync(messageId, languageId);
            if (message is null)
            {
                return NotFound();
            }

            return Ok((MessageResponse)message);
        }

        /// <summary>
        /// Update message by id.
        /// </summary>
        /// <param name="messageId">Message id.</param>
        /// <param name="languageId">Language id.</param>
        /// <param name="request">Request.</param>
        [HttpPatch("/api/v1/messages/{messageId}/{languageId}")]
        [ProducesResponseType(typeof(MessageResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<MessageResponse>> UpdateMessageByIdAsync(
            string messageId,
            string languageId,
            UpdateMessageRequest request)
        {
            var message = await messagesRepository.GetMessageByIdAsync(messageId, languageId);
            if (message is null)
            {
                return NotFound();
            }

            message.PreView = request?.PreView ?? message.PreView;
            message.Subject = request?.Subject ?? message.Subject;
            message.TemplateId = request?.TemplateId ?? message.TemplateId;
            message.Description = request?.Description ?? message.Description;
            message.ContextSms = request?.ContextSms ?? message.ContextSms;
            message.ContextHtml = request?.ContextHtml ?? message.ContextHtml;
            message.ContextText = request?.ContextText ?? message.ContextText;

            await messagesRepository.UpdateMessageAsync(message);

            return Ok((MessageResponse)message);
        }

        /// <summary>
        /// Delete message by id.
        /// </summary>
        /// <param name="messageId">Message id.</param>
        /// <param name="languageId">Language id.</param>
        [HttpDelete("/api/v1/messages/{messageId}/{languageId}")]
        [ProducesResponseType(typeof(MessageShortResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<MessageShortResponse>> DeleteMessageByIdAsync(
            string messageId,
            string languageId)
        {
            var message = await messagesRepository.GetMessageByIdAsync(messageId, languageId);
            if (message is null)
            {
                return NotFound();
            }

            await messagesRepository.DeleteMessageAsync(message);

            return Ok((MessageShortResponse)message);
        }

        /// <summary>
        /// Add message.
        /// </summary>
        /// <param name="request">Request.</param>
        [HttpPost("/api/v1/messages")]
        [ProducesResponseType(typeof(MessageResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<MessageResponse>> AddMessageAsync(
            AddMessageRequest request)
        {
            var message = await messagesRepository.GetMessageByIdAsync(request.MessageId, request.LanguageId);
            if (message != null)
            {
                return BadRequest();
            }

            message = await messagesRepository.AddMessageAsync(request);

            return Ok((MessageResponse)message);
        }
    }
}