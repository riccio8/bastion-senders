﻿using Bastion.Messages.Api.Constants;
using Bastion.Messages.Api.Models.Requests;
using Bastion.Messages.Api.Models.Responses;
using Bastion.Messages.Stores;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace Bastion.Messages.Api.Controllers
{
    /// <summary>
    /// Find and replace for messages controller.
    /// </summary>
    [ApiController]
    [Consumes(MediaTypeNames.Application.Json)]
    [Produces(MediaTypeNames.Application.Json)]
    public class FindReplaceController : ControllerBase
    {
        private readonly IMessagesService messagesService;
        private readonly IMessagesRepository messagesRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="FindReplaceController"/> class.
        /// </summary>
        public FindReplaceController(
            IMessagesService messagesService,
            IMessagesRepository messagesRepository)
        {
            this.messagesService = messagesService;
            this.messagesRepository = messagesRepository;
        }

        /// <summary>
        /// Get all find replace items.
        /// </summary>
        [HttpGet("/api/v1/messages/replaces")]
        [Authorize(Policy = MessagesPolicyConstants.ManagementPolicy)]
        public async Task<IEnumerable<FindReplaceResponse>> GetFindReplacesAsync()
        {
            var findReplaces = await messagesRepository.GetFindReplaceAsync();

            return findReplaces.Select(x => (FindReplaceResponse)x);
        }

        /// <summary>
        /// Get all global find replace items.
        /// </summary>
        [HttpGet("/api/v1/messages/replaces/global")]
        [Authorize(Policy = MessagesPolicyConstants.ManagementPolicy)]
        public IEnumerable<FindReplaceResponse> GetGlabelFindReplaces()
        {
            return messagesService.GetGlobalFindReplace()?.Select(x => (FindReplaceResponse)x);
        }

        /// <summary>
        /// Get find replace by find value.
        /// </summary>
        /// <param name="find">Find value.</param>
        [HttpGet("/api/v1/messages/replaces/{find}")]
        [Authorize(Policy = MessagesPolicyConstants.ManagementPolicy)]
        [ProducesResponseType(typeof(FindReplaceResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<FindReplaceResponse>> GetFindReplaceByFindAsync(string find)
        {
            var findReplace = await messagesRepository.GetFindReplaceByFindAsync(find);
            if (findReplace is null)
            {
                return NotFound();
            }

            return Ok((FindReplaceResponse)findReplace);
        }

        /// <summary>
        /// Delete find replace by find value.
        /// </summary>
        /// <param name="find">Find value.</param>
        [HttpDelete("/api/v1/messages/replaces/{find}")]
        [Authorize(Policy = MessagesPolicyConstants.ManagementPolicy)]
        [ProducesResponseType(typeof(FindReplaceResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<FindReplaceResponse>> DeleteFindReplaceByFindAsync(string find)
        {
            var findReplace = await messagesRepository.GetFindReplaceByFindAsync(find);
            if (findReplace is null)
            {
                return NotFound();
            }

            await messagesRepository.DeleteFindReplaceAsync(findReplace);

            return Ok((FindReplaceResponse)findReplace);
        }

        /// <summary>
        /// Update find replace by find value.
        /// </summary>
        /// <param name="find">Find value.</param>
        /// <param name="request">Request.</param>
        [HttpPatch("/api/v1/messages/replaces/{find}")]
        [Authorize(Policy = MessagesPolicyConstants.ManagementPolicy)]
        [ProducesResponseType(typeof(FindReplaceResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<FindReplaceResponse>> UpdateFindReplaceByFindAsync(string find, UpdateFindReplaceRequest request)
        {
            var findReplace = await messagesRepository.GetFindReplaceByFindAsync(find);
            if (findReplace is null)
            {
                return NotFound();
            }

            findReplace.Replace = request?.Replace ?? findReplace.Replace;

            await messagesRepository.UpdateFindReplaceAsync(findReplace);

            return Ok((FindReplaceResponse)findReplace);
        }

        /// <summary>
        /// Add find replace by find value.
        /// </summary>
        /// <param name="request">Request.</param>
        [HttpPost("/api/v1/messages/replaces")]
        [Authorize(Policy = MessagesPolicyConstants.ManagementPolicy)]
        [ProducesResponseType(typeof(FindReplaceResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<FindReplaceResponse>> AddFindReplaceByFindAsync(AddFindReplaceRequest request)
        {
            var findReplace = await messagesRepository.GetFindReplaceByFindAsync(request.Find);
            if (findReplace != null)
            {
                return BadRequest();
            }

            findReplace = await messagesRepository.AddFindReplaceAsync(request);

            return Ok((FindReplaceResponse)findReplace);
        }
    }
}