﻿using Bastion.Messages.Stores.Models;

using Microsoft.EntityFrameworkCore;

using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Bastion.Messages.EntityFrameworkCore.Extensions
{
    /// <summary>
    /// Paginated list extensions.
    /// </summary>
    public static class PaginatedListExtensions
    {
        /// <summary>
        /// To paginated list.
        /// </summary>
        /// <typeparam name="T">The list type.</typeparam>
        /// <param name="source">Source.</param>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="pageSize">Page size.</param>
        /// <param name="cancellationToken">A System.Threading.CancellationToken to observe while waiting for the task to complete.</param>
        /// <returns>Paginated list.</returns>
        public static async Task<PaginatedList<T>> ToPaginatedListAsync<T>(
            this IQueryable<T> source,
            int pageIndex,
            int pageSize,
            CancellationToken cancellationToken = default)
        {
            var totalCount = await source
                .CountAsync(cancellationToken);

            var items = await source
                .Skip(pageIndex * pageSize)
                .Take(pageSize)
                .ToListAsync(cancellationToken);

            return new PaginatedList<T>(items, totalCount, pageIndex, pageSize);
        }
    }
}