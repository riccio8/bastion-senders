﻿using Bastion.Messages.EntityFrameworkCore.Contexts;
using Bastion.Messages.EntityFrameworkCore.Repositories;
using Bastion.Messages.Stores;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection.Extensions;

using System;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Contains extension methods to <see cref="IServiceCollection"/> for configuring messages repositories.
    /// </summary>
    public static class MessagesDbContextServiceCollectionExtensions
    {
        /// <summary>
        /// Add messages repository and dbContext.
        /// </summary>
        /// <param name="optionsAction">An optional action to configure the <see cref="DbContextOptions"/> for the context.</param>
        /// <param name="services">The <see cref="IServiceCollection"/> to add services to.</param>
        /// <returns>The same service collection so that multiple calls can be chained.</returns>
        public static IServiceCollection AddMessagesRepository(
            this IServiceCollection services,
            Action<DbContextOptionsBuilder> optionsAction = null)
        {
            return services.AddMessagesRepository<MessagesRepository, MessagesDbContext>(optionsAction);
        }

        /// <summary>
        /// Add custom messages repository and dbContext.
        /// </summary>
        /// <typeparam name="TmessagesRepositary">The type of messages repositary to be registered.</typeparam>
        /// <typeparam name="TmessagesDbContext">The type of context service to be registered.</typeparam>
        /// <param name="optionsAction">An optional action to configure the <see cref="DbContextOptions"/> for the context.</param>
        /// <param name="services">The <see cref="IServiceCollection"/> to add services to.</param>
        /// <returns>The same service collection so that multiple calls can be chained.</returns>
        public static IServiceCollection AddMessagesRepository<TmessagesRepositary, TmessagesDbContext>(
            this IServiceCollection services,
            Action<DbContextOptionsBuilder> optionsAction = null)
            where TmessagesRepositary : MessagesRepository
            where TmessagesDbContext : MessagesDbContext
        {
            services.AddDbContextPool<TmessagesDbContext>(optionsAction);
            services.TryAddScoped<IMessagesRepository, TmessagesRepositary>();

            return services;
        }
    }
}