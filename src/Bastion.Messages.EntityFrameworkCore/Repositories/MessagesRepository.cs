﻿using Bastion.Messages.EntityFrameworkCore.Contexts;
using Bastion.Messages.EntityFrameworkCore.Extensions;
using Bastion.Messages.Stores;
using Bastion.Messages.Stores.Models;

using Microsoft.EntityFrameworkCore;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bastion.Messages.EntityFrameworkCore.Repositories
{
    /// <summary>
    /// Messages repository.
    /// </summary>
    public class MessagesRepository : IMessagesRepository
    {
        private readonly MessagesDbContext messageDbContext;

        /// <summary>
        /// Initializes a new instance of <see cref="MessagesRepository"/>.
        /// </summary>
        public MessagesRepository(MessagesDbContext messageDbContext)
        {
            this.messageDbContext = messageDbContext;
        }

        /// <summary>
        /// Get find and replace values.
        /// </summary>
        /// <returns>The task list of find and replace values.</returns>
        public Task<List<FindReplace>> GetFindReplaceAsync()
        {
            return messageDbContext
                .FindReplaceValues
                .AsNoTracking()
                .ToListAsync();
        }

        /// <summary>
        /// Get find and replace by find.
        /// </summary>
        /// <param name="find">Find.</param>
        /// <returns>The task of find and replace value.</returns>
        public Task<FindReplace> GetFindReplaceByFindAsync(string find)
        {
            return messageDbContext
                .FindReplaceValues
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Find == find);
        }

        /// <summary>
        /// Delete find replace.
        /// </summary>
        /// <param name="findReplace">Find and replace value.</param>
        /// <returns>The task find and replace value.</returns>
        public async Task<FindReplace> DeleteFindReplaceAsync(FindReplace findReplace)
        {
            messageDbContext.FindReplaceValues.Remove(findReplace);

            await messageDbContext.SaveChangesAsync();

            return findReplace;
        }

        /// <summary>
        /// Update find replace.
        /// </summary>
        /// <param name="findReplace">Find replace.</param>
        /// <returns>The task message.</returns>
        public async Task<FindReplace> UpdateFindReplaceAsync(FindReplace findReplace)
        {
            messageDbContext.FindReplaceValues.Update(findReplace);

            await messageDbContext.SaveChangesAsync();

            return findReplace;
        }

        /// <summary>
        /// Add find replace.
        /// </summary>
        /// <param name="findReplace">Find and replace value.</param>
        /// <returns>The task find and replace value.</returns>
        public async Task<FindReplace> AddFindReplaceAsync(FindReplace findReplace)
        {
            messageDbContext.FindReplaceValues.Add(findReplace);

            await messageDbContext.SaveChangesAsync();

            return findReplace;
        }

        /// <summary>
        /// Get templates.
        /// </summary>
        /// <returns>The task list of templates.</returns>
        public Task<List<Template>> GetTemplatesAsync()
        {
            return messageDbContext
                .Templates
                .AsNoTracking()
                .ToListAsync();
        }

        /// <summary>
        /// Get Template by id and language.
        /// </summary>
        /// <param name="templateId">Template id.</param>
        /// <param name="languageId">Language id.</param>
        /// <returns>The task template.</returns>
        public Task<Template> GetTemplateByIdAsync(string templateId, string languageId)
        {
            return messageDbContext
                .Templates
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.TemplateId == templateId && x.LanguageId == languageId);
        }

        /// <summary>
        /// Delete template.
        /// </summary>
        /// <param name="template">Template.</param>
        /// <returns>The task template.</returns>
        public async Task<Template> DeleteTemplateAsync(Template template)
        {
            messageDbContext.Templates.Remove(template);

            await messageDbContext.SaveChangesAsync();

            return template;
        }

        /// <summary>
        /// Update template.
        /// </summary>
        /// <param name="template">Template.</param>
        /// <returns>The task template.</returns>
        public async Task<Template> UpdateTemplateAsync(Template template)
        {
            messageDbContext.Templates.Update(template);

            await messageDbContext.SaveChangesAsync();

            return template;
        }

        /// <summary>
        /// Add template.
        /// </summary>
        /// <param name="template">Template.</param>
        /// <returns>The task template.</returns>
        public async Task<Template> AddTemplateAsync(Template template)
        {
            messageDbContext.Templates.Add(template);

            await messageDbContext.SaveChangesAsync();

            return template;
        }

        /// <summary>
        /// Get messages.
        /// </summary>
        /// <returns>The task list of message.</returns>
        public Task<List<Message>> GetMessagesAsync()
        {
            return messageDbContext
                .Messages
                .AsNoTracking()
                .ToListAsync();
        }

        /// <summary>
        /// Get messages paginated.
        /// </summary>
        /// <param name="search">Ssearch.</param>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="pageSize">Page size.</param>
        public Task<PaginatedList<Message>> GetMessagesPaginatedAsync(string search, int pageIndex, int pageSize)
        {
            var messages = messageDbContext
                .Messages
                .AsNoTracking();

            if (!string.IsNullOrEmpty(search))
            {
                messages = messages.Where(x => x.Subject.ToUpper().Contains(search.ToUpper()));
            }

            return messages
                .ToPaginatedListAsync(pageIndex, pageSize);
        }

        /// <summary>
        /// Get message by id and language.
        /// </summary>
        /// <param name="messageId">Message id.</param>
        /// <param name="languageId">Language id.</param>
        /// <returns>The task message.</returns>
        public Task<Message> GetMessageByIdAsync(string messageId, string languageId)
        {
            return messageDbContext
                .Messages
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.MessageId == messageId && x.LanguageId == languageId);
        }

        /// <summary>
        /// Get message by id and language.
        /// </summary>
        /// <param name="groupId">Groupid.</param>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="pageSize">Page size.</param>
        /// <returns>The task message.</returns>
        public Task<PaginatedList<Message>> GetMessagesByGroupPaginatedAsync(string groupId, int pageIndex, int pageSize)
        {
            return messageDbContext
                .Messages
                .Include(x => x.MessageType)
                .Where(x => x.MessageType.MessageGroupId == groupId)
                .AsNoTracking()
                .ToPaginatedListAsync(pageIndex, pageSize);
        }

        /// <summary>
        /// Get message by id and language.
        /// </summary>
        /// <param name="groupId">Groupid.</param>
        /// <returns>The task message.</returns>
        public Task<List<Message>> GetMessagesByGroupAsync(string groupId)
        {
            return messageDbContext
                .Messages
                .Include(x => x.MessageType)
                .Where(x => x.MessageType.MessageGroupId == groupId)
                .AsNoTracking()
                .ToListAsync();
        }

        /// <summary>
        /// Delete message.
        /// </summary>
        /// <param name="message">Message.</param>
        /// <returns>The task message.</returns>
        public async Task<Message> DeleteMessageAsync(Message message)
        {
            messageDbContext.Messages.Remove(message);
            messageDbContext.MessageType.Remove(message.MessageType);

            await messageDbContext.SaveChangesAsync();

            return message;
        }

        /// <summary>
        /// Update message.
        /// </summary>
        /// <param name="message">Message.</param>
        /// <returns>The task message.</returns>
        public async Task<Message> UpdateMessageAsync(Message message)
        {
            messageDbContext.Messages.Update(message);

            await messageDbContext.SaveChangesAsync();

            return message;
        }

        /// <summary>
        /// Add message.
        /// </summary>
        /// <param name="message">Message.</param>
        /// <returns>The task message.</returns>
        public async Task<Message> AddMessageAsync(Message message)
        {
            messageDbContext.Messages.Add(message);

            await messageDbContext.SaveChangesAsync();

            return message;
        }

        /// <summary>
        /// Get message group.
        /// </summary>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="pageSize">Page size.</param>
        /// <returns>The task message group.</returns>
        public Task<PaginatedList<MessageGroup>> GetMessageGroupPaginatedAsync(int pageIndex, int pageSize)
        {
            return messageDbContext
                .MessageGroup
                .AsNoTracking()
                .ToPaginatedListAsync(pageIndex, pageSize);
        }

        /// <summary>
        /// Get message group.
        /// </summary>
        /// <param name="groupId">Group id.</param>
        /// <returns>The task message group.</returns>
        public Task<MessageGroup> GetMessageGroupByIdAsync(string groupId)
        {
            return messageDbContext
                .MessageGroup
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.MessageGroupId == groupId);
        }

        /// <summary>
        /// Get message type.
        /// </summary>
        /// <param name="messageId">Message id.</param>
        /// <returns>The task message type.</returns>
        public Task<MessageType> GetMessageTypeByIdAsync(string messageId)
        {
            return messageDbContext
                .MessageType
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.MessageId == messageId);
        }

        /// <summary>
        /// Delete message group.
        /// </summary>
        /// <param name="messageGroup">Message group.</param>
        /// <returns>The task message group.</returns>
        public async Task<MessageGroup> DeleteMessageGroupAsync(MessageGroup messageGroup)
        {
            messageDbContext.MessageGroup.Remove(messageGroup);

            await messageDbContext.SaveChangesAsync();

            return messageGroup;
        }

        /// <summary>
        /// Update message group.
        /// </summary>
        /// <param name="messageGroup">Message group.</param>
        /// <returns>The task message group.</returns>
        public async Task<MessageGroup> UpdateMessageGroupAsync(MessageGroup messageGroup)
        {
            messageDbContext.MessageGroup.Update(messageGroup);

            await messageDbContext.SaveChangesAsync();

            return messageGroup;
        }

        /// <summary>
        /// Add mesage group.
        /// </summary>
        /// <param name="messageGroup">Message group.</param>
        /// <returns>The task message group.</returns>
        public async Task<MessageGroup> AddMessageGroupAsync(MessageGroup messageGroup)
        {
            messageDbContext.MessageGroup.Add(messageGroup);

            await messageDbContext.SaveChangesAsync();

            return messageGroup;
        }

        /// <summary>
        /// Update message type.
        /// </summary>
        /// <param name="messageType">Message type.</param>
        /// <returns>The task message type.</returns>
        public async Task<MessageType> UpdateMessageTypeAsync(MessageType messageType)
        {
            messageDbContext.MessageType.Update(messageType);

            await messageDbContext.SaveChangesAsync();

            return messageType;
        }
    }
}