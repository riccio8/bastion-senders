﻿using Bastion.Messages.Stores.Models;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bastion.Messages.EntityFrameworkCore.Configurations
{
    internal class MessageConfiguration : IEntityTypeConfiguration<Message>
    {
        public void Configure(EntityTypeBuilder<Message> builder)
        {
            builder.HasKey(e => new { e.MessageId, e.LanguageId })
                    .HasName("messages_pkey");

            builder.ToTable("messages", "message");

            builder.Property(e => e.MessageId)
                .HasColumnName("message_id")
                .HasMaxLength(36);

            builder.Property(e => e.LanguageId)
                .HasColumnName("language_id")
                .HasMaxLength(3)
                .HasDefaultValueSql("'en'::character varying");

            builder.Property(e => e.ContextHtml)
                .HasColumnName("context_html");

            builder.Property(e => e.ContextSms)
                .HasColumnName("context_sms")
                .HasMaxLength(255);

            builder.Property(e => e.ContextText)
                 .HasColumnName("context_text");

            builder.Property(e => e.Description)
                .HasColumnName("description")
                .HasMaxLength(255);

            builder.Property(e => e.ModificationAt)
                .HasColumnName("modification_ts")
                .HasColumnType("timestamp(6) without time zone")
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Property(e => e.PreView)
                .HasColumnName("pre_view")
                .HasMaxLength(255);

            builder.Property(e => e.Subject)
                .HasColumnName("subject")
                .HasMaxLength(255);

            builder.Property(e => e.TemplateId)
                .HasColumnName("template_id")
                .HasMaxLength(36);

            builder.HasOne(d => d.MessageType)
                .WithMany(p => p.Messages)
                .HasForeignKey(d => d.MessageId)
                .OnDelete(DeleteBehavior.SetNull)
                .HasConstraintName("messages_type_fkey");

            builder.HasOne(d => d.Template)
                .WithMany(p => p.Messages)
                .HasForeignKey(d => new { d.TemplateId, d.LanguageId })
                .OnDelete(DeleteBehavior.SetNull)
                .HasConstraintName("messages_template_fkey");
        }
    }
}