﻿using Bastion.Messages.Stores.Models;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bastion.Messages.EntityFrameworkCore.Configurations
{
    internal class TemplateConfiguration : IEntityTypeConfiguration<Template>
    {
        public void Configure(EntityTypeBuilder<Template> builder)
        {
            builder.HasKey(e => new { e.TemplateId, e.LanguageId })
                    .HasName("templates_pkey");

            builder.ToTable("templates", "message");

            builder.Property(e => e.TemplateId)
                .HasColumnName("template_id")
                .HasMaxLength(36);

            builder.Property(e => e.LanguageId)
                .HasColumnName("language_id")
                .HasMaxLength(3);

            builder.Property(e => e.Footer)
                .HasColumnName("footer");

            builder.Property(e => e.Header)
                .HasColumnName("header");

            builder.Property(e => e.ModificationAt)
                .HasColumnName("modification_ts")
                .HasColumnType("timestamp(6) without time zone")
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();
        }
    }
}