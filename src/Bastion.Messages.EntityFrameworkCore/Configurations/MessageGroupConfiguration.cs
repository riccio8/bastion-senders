﻿using Bastion.Messages.Stores.Models;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bastion.Messages.EntityFrameworkCore.Configurations
{
    internal class MessageGroupConfiguration : IEntityTypeConfiguration<MessageGroup>
    {
        public void Configure(EntityTypeBuilder<MessageGroup> builder)
        {
            builder.ToTable("message_group", "message");

            builder.Property(e => e.MessageGroupId)
                .HasColumnName("message_group_id")
                .HasMaxLength(36);

            builder.Property(e => e.Description)
                .HasColumnName("description")
                .HasMaxLength(255);
        }
    }
}