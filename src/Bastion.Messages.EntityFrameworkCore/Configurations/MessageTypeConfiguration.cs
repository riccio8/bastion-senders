﻿using Bastion.Messages.Stores.Models;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bastion.Messages.EntityFrameworkCore.Configurations
{
    internal class MessageTypeConfiguration : IEntityTypeConfiguration<MessageType>
    {
        public void Configure(EntityTypeBuilder<MessageType> builder)
        {
            builder.HasKey(e => e.MessageId)
                .HasName("message_type_pkey");

            builder.ToTable("message_type", "message");

            builder.Property(e => e.MessageId)
                .HasColumnName("message_id")
                .HasMaxLength(36)
               .ValueGeneratedNever();

            builder.Property(e => e.Description)
                .HasColumnName("description")
                .HasMaxLength(255);

            builder.Property(e => e.MessageGroupId)
                .HasColumnName("message_group_id")
                .HasMaxLength(36);

            builder.HasOne(d => d.MessageGroup)
                .WithMany(p => p.MessageTypes)
                .HasForeignKey(d => d.MessageGroupId)
                .OnDelete(DeleteBehavior.SetNull)
                .HasConstraintName("message_type_fkey");
        }
    }
}