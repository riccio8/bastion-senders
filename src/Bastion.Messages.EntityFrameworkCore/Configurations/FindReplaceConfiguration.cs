﻿using Bastion.Messages.Stores.Models;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bastion.Messages.EntityFrameworkCore.Configurations
{
    internal class FindReplaceConfiguration : IEntityTypeConfiguration<FindReplace>
    {
        public void Configure(EntityTypeBuilder<FindReplace> builder)
        {
            builder.HasKey(e => e.Find)
                    .HasName("find_replace_values_pkey");

            builder.ToTable("find_replace_values", "message");

            builder.Property(e => e.Find)
                .HasColumnName("find")
                .HasMaxLength(36);

            builder.Property(e => e.ModificationAt)
                .HasColumnName("modification_ts")
                .HasColumnType("timestamp(6) without time zone")
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Property(e => e.Replace)
                .IsRequired()
                .HasColumnName("replace");
        }
    }
}