﻿using Bastion.Messages.EntityFrameworkCore.Configurations;
using Bastion.Messages.Stores.Models;

using Microsoft.EntityFrameworkCore;

namespace Bastion.Messages.EntityFrameworkCore.Contexts
{
    /// <summary>
    /// Messages data base context.
    /// </summary>
    public partial class MessagesDbContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of <see cref="MessagesDbContext"/>.
        /// </summary>
        public MessagesDbContext(DbContextOptions<MessagesDbContext> options)
            : base(options)
        {
        }

        /// <summary>
        /// Find replace values.
        /// </summary>
        public virtual DbSet<FindReplace> FindReplaceValues { get; set; }

        /// <summary>
        /// Messages groups.
        /// </summary>
        public virtual DbSet<MessageGroup> MessageGroup { get; set; }

        /// <summary>
        /// Messages type.
        /// </summary>
        public virtual DbSet<MessageType> MessageType { get; set; }

        /// <summary>
        /// Messages.
        /// </summary>
        public virtual DbSet<Message> Messages { get; set; }

        /// <summary>
        /// Templates.
        /// </summary>
        public virtual DbSet<Template> Templates { get; set; }

        /// <summary>
        /// Model creating.
        /// </summary>
        /// <param name="modelBuilder">The builder being used to construct the model for this context.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new MessageConfiguration());
            modelBuilder.ApplyConfiguration(new TemplateConfiguration());
            modelBuilder.ApplyConfiguration(new FindReplaceConfiguration());
            modelBuilder.ApplyConfiguration(new MessageGroupConfiguration());
            modelBuilder.ApplyConfiguration(new MessageTypeConfiguration());
        }
    }
}