using Bastion.Messages.Api.Constants;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Claims;

namespace BastionSendersSample.Api
{
    /// <summary>
    /// Startup.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// </summary>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// Configuration.
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// ConfigureServices.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            // ===== Add Bastion Messages service and repository ========
            services.AddMessagesService()
                .AddMessagesRepository(options => options.UseInMemoryDatabase("InMemoryDbForTesting"));

            services.AddControllers();

            // ===== Add basic authentication ========
            services.AddBasicAuthentication();
            services.AddAuthorization(options => options.AddPolicy(MessagesPolicyConstants.ManagementPolicy,
                policy => policy.RequireClaim(ClaimTypes.NameIdentifier, "admin")));

            // ===== Add swagger ========
            services.AddSwaggerGen(options =>
            {
                options.EnableAnnotations();
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "API",
                    Version = "v1",
                    Description = $"Version: <b>{Assembly.GetEntryAssembly().GetName().Version}</b>"
                });

                Directory.GetFiles(AppContext.BaseDirectory, "*.xml").ToList().ForEach(file => options.IncludeXmlComments(file, true));

                options.AddBasicSecurityDefinition();
            });
        }

        /// <summary>
        /// Configure.
        /// </summary>
        /// <param name="app">Application builder.</param>
        /// <param name="env">WebHost environment.</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();
            app.UseSwaggerUI(configure =>
            {
                configure.RoutePrefix = string.Empty;
                configure.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });
        }
    }
}