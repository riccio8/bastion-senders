﻿namespace Bastion.Authentication.Basic
{
    public static class BasicAuthenticationDefaults
    {
        public const string AuthenticationScheme = "Basic";

        public const string AuthenticationDisplayName = "Basic";

        public const string AuthorizationHeaderName = "Authorization";
    }
}