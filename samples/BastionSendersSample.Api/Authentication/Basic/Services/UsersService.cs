﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bastion.Authentication.Basic.Services
{
    public interface IUserService
    {
        Task<User> Authenticate(string username, string password);
    }

    public class UserService : IUserService
    {
        private readonly List<User> users = new List<User>
        {
            new User { Username = "user", Password = "user" },
            new User { Username = "admin", Password = "admin" },
        };

        public async Task<User> Authenticate(string username, string password)
        {
            var user = await Task.Run(() => users.SingleOrDefault(x => x.Username == username && x.Password == password));

            return user?.WithoutPassword();
        }
    }

    public static class ExtensionMethods
    {
        public static User WithoutPassword(this User user)
        {
            user.Password = null;
            return user;
        }
    }

    public class User
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}