﻿using Bastion.Authentication.Basic;
using Bastion.Authentication.Basic.Services;

using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.OpenApi.Models;

using Swashbuckle.AspNetCore.Filters;
using Swashbuckle.AspNetCore.SwaggerGen;

using System;
using System.Collections.Generic;
using System.Linq;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class BasicAuthenticationExtensions
    {
        public static AuthenticationBuilder AddBasicAuthentication(
            this AuthenticationBuilder builder)
        {
            return builder.AddBasicAuthentication(BasicAuthenticationDefaults.AuthenticationScheme, _ => { });
        }

        public static AuthenticationBuilder AddBasicAuthentication(
            this AuthenticationBuilder builder,
            string authenticationScheme,
            Action<BasicAuthenticationOptions> configureOptions)
        {
            return builder.AddBasicAuthentication(authenticationScheme, BasicAuthenticationDefaults.AuthenticationDisplayName, configureOptions);
        }

        public static AuthenticationBuilder AddBasicAuthentication(
            this AuthenticationBuilder builder,
            string authenticationScheme,
            string displayName,
            Action<BasicAuthenticationOptions> configureOptions)
        {
            return builder.AddScheme<BasicAuthenticationOptions, BasicAuthenticationHandler>(authenticationScheme, displayName, configureOptions);
        }

        public static IServiceCollection AddBasicAuthentication(this IServiceCollection services)
        {
            services.TryAddScoped<IUserService, UserService>();

            services.AddAuthentication(BasicAuthenticationDefaults.AuthenticationScheme)
                .AddBasicAuthentication();

            return services;
        }
    }

    public static class SwaggerGenOptionsExtensions
    {
        public static SwaggerGenOptions AddBasicSecurityDefinition(this SwaggerGenOptions options)
        {
            options.OperationFilter<SwaggerAuthorizeCheckOperationFilter>();
            options.OperationFilter<AppendAuthorizeToSummaryOperationFilter>();
            options.AddSecurityDefinition(BasicAuthenticationDefaults.AuthenticationScheme, new OpenApiSecurityScheme
            {
                In = ParameterLocation.Header,
                Type = SecuritySchemeType.Http,
                Description = "Basic auth added to authorization header",
                Name = BasicAuthenticationDefaults.AuthorizationHeaderName,
                Scheme = BasicAuthenticationDefaults.AuthenticationScheme.ToLowerInvariant(),
            });

            return options;
        }
    }

    public class SwaggerAuthorizeCheckOperationFilter : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            if (operation is null || context is null) return;

            var hasAuthorize = context.MethodInfo.DeclaringType.GetCustomAttributes(true).OfType<AuthorizeAttribute>().Any() ||
                               context.MethodInfo.GetCustomAttributes(true).OfType<AuthorizeAttribute>().Any();

            if (!hasAuthorize) return;

            operation.Security = new List<OpenApiSecurityRequirement>
            {
               new OpenApiSecurityRequirement
               {
                   {
                       new OpenApiSecurityScheme
                       {
                           Reference = new OpenApiReference
                           {
                               Type = ReferenceType.SecurityScheme,
                               Id = BasicAuthenticationDefaults.AuthenticationScheme
                           }
                       }, Array.Empty<string>()
                   }
               }
            };
        }
    }
}