using System.Diagnostics.CodeAnalysis;

using Xunit;

namespace BastionSendersSample.Api.FunctionalTests
{
    [ExcludeFromCodeCoverage]
    public class VerifyAccessScenario : IClassFixture<ScenarioFactory<Startup>>
    {
        private const string loginUser = "user:user";
        private const string loginAdmin = "admin:admin";

        private readonly ScenarioFactory<Startup> factory;

        public VerifyAccessScenario(ScenarioFactory<Startup> factory)
        {
            this.factory = factory;
        }
    }
}