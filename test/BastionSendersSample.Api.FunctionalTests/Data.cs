﻿using Bastion.Messages.Stores.Models;

using System;
using System.Collections.Generic;

using Xunit;

namespace BastionSendersSample.Api.FunctionalTests
{
    internal class Data
    {
        public static readonly FindReplace findReplace = new FindReplace()
        {
            Find = "Find",
            ModificationAt = DateTime.Now,
            Replace = "Replace"
        };

        public static readonly Template template = new Template()
        {
            Header = "Header",
            Footer = "Footer",
            TemplateId = "TemplateId",
            LanguageId = "LanguageId",
            ModificationAt = DateTime.Now
        };

        public static readonly Message message = new Message()
        {
            MessageId = "MessageId",
            LanguageId = "LanguageId",
            TemplateId = "TemplateId",
            PreView = "PreView",
            Subject = "Subject",
            ContextHtml = "ContextHtml",
            ContextText = "ContextText",
            ContextSms = "ContextSms",
            ModificationAt = DateTime.Now,
            Description = "Description",
            MessageType = messageType
        };

        public static readonly MessageType messageType = new MessageType()
        {
            MessageId = "MessageId",
            MessageGroupId = "MessageGroupId",
            MessageGroup = messageGroup,
            Description = "Description",
            Messages = new List<Message> { message },
        };

        public static readonly MessageGroup messageGroup = new MessageGroup()
        {
            MessageGroupId = "MessageGroupId",
            Description = "Description",
            MessageTypes = new List<MessageType> { messageType },
        };

        public static TheoryData<FindReplace> GetFindReplace()
        {
            return new TheoryData<FindReplace> { findReplace };
        }

        public static TheoryData<Template> GetTemplate()
        {
            return new TheoryData<Template> { template };
        }

        public static TheoryData<Message> GetMessage()
        {
            return new TheoryData<Message> { message };
        }

        public static TheoryData<MessageType> GetMessageType()
        {
            return new TheoryData<MessageType> { messageType };
        }

        public static TheoryData<MessageGroup> GetMessageGroup()
        {
            return new TheoryData<MessageGroup> { messageGroup };
        }

        public static TheoryData<List<FindReplace>> GetListFindReplace()
        {
            return new TheoryData<List<FindReplace>> { new List<FindReplace> { findReplace, findReplace } };
        }

        public static TheoryData<Message, List<FindReplace>> GetMessageListFindReplace()
        {
            return new TheoryData<Message, List<FindReplace>> { { message, new List<FindReplace> { findReplace, findReplace } } };
        }

        public static TheoryData<Message, List<FindReplace>, Template> GetMessageFindReplaceTemplate()
        {
            return new TheoryData<Message, List<FindReplace>, Template> { { message, new List<FindReplace> { findReplace, findReplace }, template } };
        }

        public static TheoryData<Message, Template> GetMessageTemplate()
        {
            return new TheoryData<Message, Template> { { message, template } };
        }
    }
}