using System.Diagnostics.CodeAnalysis;

using Xunit;

namespace BastionSendersSample.Api.FunctionalTests
{
    [ExcludeFromCodeCoverage]
    public class AnonymousScenario : IClassFixture<ScenarioFactory<Startup>>
    {
        private readonly ScenarioFactory<Startup> factory;

        public AnonymousScenario(ScenarioFactory<Startup> factory)
        {
            this.factory = factory;
        }
    }
}