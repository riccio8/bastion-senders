﻿using Bastion.Messages.EntityFrameworkCore.Contexts;
using Bastion.Messages.EntityFrameworkCore.Repositories;
using Bastion.Messages.Stores.Models;

using Microsoft.EntityFrameworkCore;

using System;
using System.Linq;
using System.Threading.Tasks;

using Xunit;

namespace BastionSendersSample.Api.FunctionalTests
{
    public class MessagesRepositoryTests
    {
        [Theory(DisplayName = "Repository get templates test.")]
        [MemberData(nameof(Data.GetTemplate), MemberType = typeof(Data))]
        public async Task GetTemplatesTest(Template template)
        {
            var options = new DbContextOptionsBuilder<MessagesDbContext>()
               .UseInMemoryDatabase(databaseName: "GetTemplatesTest")
               .Options;

            using (var context = new MessagesDbContext(options))
            {
                context.Templates.Add(template);
                context.SaveChanges();
                var messagesRepository = new MessagesRepository(context);
                var templates = await messagesRepository.GetTemplatesAsync();
                Assert.NotNull(templates);
                Assert.NotEmpty(templates);

                Assert.Equal(templates.FirstOrDefault().Header, template.Header);
            }
        }

        [Theory(DisplayName = "Repository get template by id test.")]
        [MemberData(nameof(Data.GetTemplate), MemberType = typeof(Data))]
        public async Task GetTemplateByIdTest(Template template)
        {
            var options = new DbContextOptionsBuilder<MessagesDbContext>()
               .UseInMemoryDatabase(databaseName: "GetTemplateByIdTests")
               .Options;

            using (var context = new MessagesDbContext(options))
            {
                context.Templates.Add(template);
                context.SaveChanges();
                var messagesRepository = new MessagesRepository(context);
                var templateRezult = await messagesRepository.GetTemplateByIdAsync(template.TemplateId, template.LanguageId);
                Assert.NotNull(templateRezult);
                Assert.Equal(templateRezult.TemplateId, templateRezult.TemplateId);
                Assert.Equal(templateRezult.LanguageId, templateRezult.LanguageId);
            }
        }

        [Theory(DisplayName = "Repository add template test.")]
        [MemberData(nameof(Data.GetTemplate), MemberType = typeof(Data))]
        public async Task AddTemplateTest(Template template)
        {
            var options = new DbContextOptionsBuilder<MessagesDbContext>()
               .UseInMemoryDatabase(databaseName: "AddTemplateTests")
               .Options;

            using (var context = new MessagesDbContext(options))
            {
                var messagesRepository = new MessagesRepository(context);
                var templateRezult = await messagesRepository.AddTemplateAsync(template);
                Assert.NotNull(templateRezult);
                Assert.Equal(templateRezult.TemplateId, templateRezult.TemplateId);
                Assert.Equal(templateRezult.LanguageId, templateRezult.LanguageId);
            }
        }

        [Theory(DisplayName = "Repository update template test.")]
        [MemberData(nameof(Data.GetTemplate), MemberType = typeof(Data))]
        public async Task UpdateTemplateTest(Template template)
        {
            var options = new DbContextOptionsBuilder<MessagesDbContext>()
               .UseInMemoryDatabase(databaseName: "UpdateTemplateTests")
               .Options;

            using (var context = new MessagesDbContext(options))
            {
                context.Templates.Add(template);
                context.SaveChanges();

                template.ModificationAt = DateTime.Now.AddDays(1);
                var messagesRepository = new MessagesRepository(context);
                var templateRezult = await messagesRepository.UpdateTemplateAsync(template);
                Assert.NotNull(templateRezult);
                Assert.Equal(templateRezult.TemplateId, templateRezult.TemplateId);
                Assert.Equal(templateRezult.ModificationAt, templateRezult.ModificationAt);
            }
        }

        [Theory(DisplayName = "Repository delete template test.")]
        [MemberData(nameof(Data.GetTemplate), MemberType = typeof(Data))]
        public async Task DeleteTemplateTest(Template template)
        {
            var options = new DbContextOptionsBuilder<MessagesDbContext>()
               .UseInMemoryDatabase(databaseName: "GetTemplatesAsync")
               .Options;

            using (var context = new MessagesDbContext(options))
            {
                context.Templates.Add(template);
                context.SaveChanges();

                var messagesRepository = new MessagesRepository(context);
                var templateRezult = await messagesRepository.DeleteTemplateAsync(template);
                Assert.NotNull(templateRezult);
                Assert.Equal(templateRezult.TemplateId, templateRezult.TemplateId);
                Assert.Equal(templateRezult.ModificationAt, templateRezult.ModificationAt);
            }
        }

        [Theory(DisplayName = "Repository get message test.")]
        [MemberData(nameof(Data.GetMessage), MemberType = typeof(Data))]
        public async Task GetMessagesTest(Message message)
        {
            var options = new DbContextOptionsBuilder<MessagesDbContext>()
               .UseInMemoryDatabase(databaseName: "GetMessagesTest")
               .Options;

            using (var context = new MessagesDbContext(options))
            {
                context.Messages.Add(message);
                context.SaveChanges();
                var messagesRepository = new MessagesRepository(context);
                var messages = await messagesRepository.GetMessagesAsync();
                Assert.NotNull(messages);
                Assert.NotEmpty(messages);

                Assert.Equal(messages.FirstOrDefault().MessageId, message.MessageId);
            }
        }

        [Theory(DisplayName = "Repository get message by id test.")]
        [MemberData(nameof(Data.GetMessage), MemberType = typeof(Data))]
        public async Task GetMessageByIdTest(Message message)
        {
            var options = new DbContextOptionsBuilder<MessagesDbContext>()
              .UseInMemoryDatabase(databaseName: "GetMessageByIdTests")
              .Options;

            using (var context = new MessagesDbContext(options))
            {
                context.Messages.Add(message);
                context.SaveChanges();
                var messagesRepository = new MessagesRepository(context);
                var messages = await messagesRepository.GetMessageByIdAsync(message.MessageId, message.LanguageId);
                Assert.NotNull(messages);
                Assert.Equal(messages.MessageId, message.MessageId);
            }
        }

        [Theory(DisplayName = "Repository get message by group test.")]
        [MemberData(nameof(Data.GetMessageType), MemberType = typeof(Data))]
        public async Task GetMessagesByGroupTest(MessageType messageType)
        {
            var options = new DbContextOptionsBuilder<MessagesDbContext>()
               .UseInMemoryDatabase(databaseName: "GetMessagesByGroup")
               .Options;

            using (var context = new MessagesDbContext(options))
            {
                context.MessageType.Add(messageType);
                context.SaveChanges();

                var messagesRepository = new MessagesRepository(context);
                var messages = await messagesRepository.GetMessagesByGroupAsync(messageType.MessageGroupId);
                Assert.NotNull(messages);
                Assert.NotEmpty(messages);
                Assert.Equal(messages.FirstOrDefault().MessageId, messageType.MessageId);
            }
        }

        [Theory(DisplayName = "Repository add message test.")]
        [MemberData(nameof(Data.GetMessage), MemberType = typeof(Data))]
        public async Task AddMessageTest(Message message)
        {
            var options = new DbContextOptionsBuilder<MessagesDbContext>()
               .UseInMemoryDatabase(databaseName: "AddMessageTests")
               .Options;

            using (var context = new MessagesDbContext(options))
            {
                var messagesRepository = new MessagesRepository(context);
                var messageRezult = await messagesRepository.AddMessageAsync(message);
                Assert.NotNull(messageRezult);
                Assert.Equal(messageRezult.TemplateId, message.TemplateId);
                Assert.Equal(messageRezult.LanguageId, message.LanguageId);
            }
        }

        [Theory(DisplayName = "Repository update message test.")]
        [MemberData(nameof(Data.GetMessage), MemberType = typeof(Data))]
        public async Task UpdateMessageTest(Message message)
        {
            var options = new DbContextOptionsBuilder<MessagesDbContext>()
               .UseInMemoryDatabase(databaseName: "UpdateMessageTests")
               .Options;

            using (var context = new MessagesDbContext(options))
            {
                context.Messages.Add(message);
                context.SaveChanges();

                message.ModificationAt = DateTime.Now.AddDays(1);
                var messagesRepository = new MessagesRepository(context);
                var messageRezult = await messagesRepository.UpdateMessageAsync(message);
                Assert.NotNull(messageRezult);
                Assert.Equal(messageRezult.TemplateId, message.TemplateId);
                Assert.Equal(messageRezult.ModificationAt, message.ModificationAt);
            }
        }

        [Theory(DisplayName = "Repository delete message test.")]
        [MemberData(nameof(Data.GetMessage), MemberType = typeof(Data))]
        public async Task DeleteMessageTest(Message message)
        {
            var options = new DbContextOptionsBuilder<MessagesDbContext>()
               .UseInMemoryDatabase(databaseName: "DeleteMessageTests")
               .Options;

            using (var context = new MessagesDbContext(options))
            {
                context.Messages.Add(message);
                context.SaveChanges();

                var messagesRepository = new MessagesRepository(context);
                var messageRezult = await messagesRepository.DeleteMessageAsync(message);
                Assert.NotNull(messageRezult);
                Assert.Equal(messageRezult.TemplateId, message.TemplateId);
                Assert.Equal(messageRezult.ModificationAt, message.ModificationAt);
            }
        }

        [Theory(DisplayName = "Repository get message by id test.")]
        [MemberData(nameof(Data.GetMessageType), MemberType = typeof(Data))]
        public async Task GetMessageTypeByIdTest(MessageType messageType)
        {
            var options = new DbContextOptionsBuilder<MessagesDbContext>()
               .UseInMemoryDatabase(databaseName: "GetMessageTypeById")
               .Options;

            using (var context = new MessagesDbContext(options))
            {
                context.MessageType.Add(messageType);
                context.SaveChanges();
                var messagesRepository = new MessagesRepository(context);
                var messageTypeRezult = await messagesRepository.GetMessageTypeByIdAsync(messageType.MessageId);
                Assert.NotNull(messageTypeRezult);
                Assert.Equal(messageTypeRezult.MessageId, messageType.MessageId);
                Assert.Equal(messageTypeRezult.MessageGroupId, messageType.MessageGroupId);
            }
        }

        [Theory(DisplayName = "Repository update message type test.")]
        [MemberData(nameof(Data.GetMessageType), MemberType = typeof(Data))]
        public async Task UpdateMessageTypeTest(MessageType messageType)
        {
            var options = new DbContextOptionsBuilder<MessagesDbContext>()
               .UseInMemoryDatabase(databaseName: "UpdateMessageTypeTests")
               .Options;

            using (var context = new MessagesDbContext(options))
            {
                context.MessageType.Add(messageType);
                context.SaveChanges();

                messageType.Description = "Description";
                var messagesRepository = new MessagesRepository(context);
                var messageTypeRezult = await messagesRepository.UpdateMessageTypeAsync(messageType);
                Assert.NotNull(messageTypeRezult);
                Assert.Equal(messageTypeRezult.MessageGroupId, messageType.MessageGroupId);
                Assert.Equal(messageTypeRezult.MessageId, messageType.MessageId);
            }
        }

        [Theory(DisplayName = "Repository get message group by id test.")]
        [MemberData(nameof(Data.GetMessageGroup), MemberType = typeof(Data))]
        public async Task GetMessageGroupByIdTest(MessageGroup messageGroup)
        {
            var options = new DbContextOptionsBuilder<MessagesDbContext>()
               .UseInMemoryDatabase(databaseName: "GetMessageGroupByIdTest")
               .Options;

            using (var context = new MessagesDbContext(options))
            {
                context.MessageGroup.Add(messageGroup);
                context.SaveChanges();

                var messagesRepository = new MessagesRepository(context);
                var messagesRezult = await messagesRepository.GetMessageGroupByIdAsync(messageGroup.MessageGroupId);
                Assert.NotNull(messagesRezult);
                Assert.Equal(messagesRezult.MessageGroupId, messageGroup.MessageGroupId);
            }
        }

        [Theory(DisplayName = "Repository add message group test.")]
        [MemberData(nameof(Data.GetMessageGroup), MemberType = typeof(Data))]
        public async Task AddMessageGroupTest(MessageGroup messageGroup)
        {
            var options = new DbContextOptionsBuilder<MessagesDbContext>()
               .UseInMemoryDatabase(databaseName: "AddMessageGroupTests")
               .Options;

            using (var context = new MessagesDbContext(options))
            {
                var messagesRepository = new MessagesRepository(context);
                var messageGroupRezult = await messagesRepository.AddMessageGroupAsync(messageGroup);
                Assert.NotNull(messageGroupRezult);
                Assert.Equal(messageGroupRezult.MessageGroupId, messageGroup.MessageGroupId);
            }
        }

        [Theory(DisplayName = "Repository update message group test.")]
        [MemberData(nameof(Data.GetMessageGroup), MemberType = typeof(Data))]
        public async Task UpdateMessageGroupTest(MessageGroup messageGroup)
        {
            var options = new DbContextOptionsBuilder<MessagesDbContext>()
               .UseInMemoryDatabase(databaseName: "UpdateMessageGroupTests")
               .Options;

            using (var context = new MessagesDbContext(options))
            {
                context.MessageGroup.Add(messageGroup);
                context.SaveChanges();

                messageGroup.Description = "Description";
                var messagesRepository = new MessagesRepository(context);
                var messageGroupRezult = await messagesRepository.UpdateMessageGroupAsync(messageGroup);
                Assert.NotNull(messageGroupRezult);
                Assert.Equal(messageGroupRezult.MessageGroupId, messageGroup.MessageGroupId);
            }
        }

        [Theory(DisplayName = "Repository delete message group test.")]
        [MemberData(nameof(Data.GetMessageGroup), MemberType = typeof(Data))]
        public async Task DeleteMessageGroupTest(MessageGroup messageGroup)
        {
            var options = new DbContextOptionsBuilder<MessagesDbContext>()
               .UseInMemoryDatabase(databaseName: "DeleteMessageGroupTests")
               .Options;

            using (var context = new MessagesDbContext(options))
            {
                context.MessageGroup.Add(messageGroup);
                context.SaveChanges();

                var messagesRepository = new MessagesRepository(context);
                var messageGroupRezult = await messagesRepository.DeleteMessageGroupAsync(messageGroup);
                Assert.NotNull(messageGroupRezult);
                Assert.Equal(messageGroupRezult.MessageGroupId, messageGroup.MessageGroupId);
            }
        }

        [Theory(DisplayName = "Repository get find replace test.")]
        [MemberData(nameof(Data.GetFindReplace), MemberType = typeof(Data))]
        public async Task GetFindReplaceTest(FindReplace findReplace)
        {
            var options = new DbContextOptionsBuilder<MessagesDbContext>()
               .UseInMemoryDatabase(databaseName: "GetMessageGroupByIdTest")
               .Options;

            using (var context = new MessagesDbContext(options))
            {
                context.FindReplaceValues.Add(findReplace);
                context.SaveChanges();

                var messagesRepository = new MessagesRepository(context);
                var findReplaceRezult = await messagesRepository.GetFindReplaceAsync();
                Assert.NotNull(findReplaceRezult);
                Assert.NotEmpty(findReplaceRezult);
                Assert.Equal(findReplaceRezult.FirstOrDefault().ModificationAt, findReplace.ModificationAt);
            }
        }

        [Theory(DisplayName = "Repository get find replace by find test.")]
        [MemberData(nameof(Data.GetFindReplace), MemberType = typeof(Data))]
        public async Task GetFindReplaceByFindTest(FindReplace findReplace)
        {
            var options = new DbContextOptionsBuilder<MessagesDbContext>()
               .UseInMemoryDatabase(databaseName: "GetFindReplaceByFindTest")
               .Options;

            using (var context = new MessagesDbContext(options))
            {
                context.FindReplaceValues.Add(findReplace);
                context.SaveChanges();

                var messagesRepository = new MessagesRepository(context);
                var findReplaceRezult = await messagesRepository.GetFindReplaceByFindAsync(findReplace.Find);
                Assert.NotNull(findReplaceRezult);

                Assert.Equal(findReplaceRezult.Find, findReplace.Find);
            }
        }

        [Theory(DisplayName = "Repository delete find replace test.")]
        [MemberData(nameof(Data.GetFindReplace), MemberType = typeof(Data))]
        public async Task DeleteFindReplaceTest(FindReplace findReplace)
        {
            var options = new DbContextOptionsBuilder<MessagesDbContext>()
               .UseInMemoryDatabase(databaseName: "DeleteFindReplaceTest")
               .Options;

            using (var context = new MessagesDbContext(options))
            {
                context.FindReplaceValues.Add(findReplace);
                context.SaveChanges();

                var messagesRepository = new MessagesRepository(context);
                var findReplaceRezult = await messagesRepository.DeleteFindReplaceAsync(findReplace);
                Assert.NotNull(findReplaceRezult);

                Assert.Equal(findReplaceRezult.Find, findReplace.Find);
            }
        }

        [Theory(DisplayName = "Repository add find replace test.")]
        [MemberData(nameof(Data.GetFindReplace), MemberType = typeof(Data))]
        public async Task AddFindReplaceTest(FindReplace findReplace)
        {
            var options = new DbContextOptionsBuilder<MessagesDbContext>()
               .UseInMemoryDatabase(databaseName: "AddMessageGroupTest")
               .Options;

            using (var context = new MessagesDbContext(options))
            {
                var messagesRepository = new MessagesRepository(context);
                var findReplaceRezult = await messagesRepository.AddFindReplaceAsync(findReplace);
                Assert.NotNull(findReplaceRezult);
                Assert.Equal(findReplaceRezult.Find, findReplace.Find);
            }
        }

        [Theory(DisplayName = "Repository update find replace test.")]
        [MemberData(nameof(Data.GetFindReplace), MemberType = typeof(Data))]
        public async Task UpdateFindReplaceTest(FindReplace findReplace)
        {
            var options = new DbContextOptionsBuilder<MessagesDbContext>()
               .UseInMemoryDatabase(databaseName: "UpdateMessageGroupTest")
               .Options;

            using (var context = new MessagesDbContext(options))
            {
                context.FindReplaceValues.Add(findReplace);
                context.SaveChanges();

                findReplace.ModificationAt = DateTime.Now;
                var messagesRepository = new MessagesRepository(context);

                var findReplaceRezult = await messagesRepository.UpdateFindReplaceAsync(findReplace);
                Assert.NotNull(findReplaceRezult);
                Assert.Equal(findReplaceRezult.ModificationAt, findReplace.ModificationAt);
            }
        }
    }
}