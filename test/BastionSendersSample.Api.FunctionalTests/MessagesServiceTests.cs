﻿using Bastion.Messages.Stores.Models;

using FakeItEasy;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Xunit;

namespace BastionSendersSample.Api.FunctionalTests
{
    public class MessagesServiceTests : TestBase
    {
        [Theory(DisplayName = "Service get message test.")]
        [MemberData(nameof(Data.GetMessage), MemberType = typeof(Data))]
        public async Task GetMessagesTest(Message message)
        {
            (var service, var repository) = GetMessagesService();
            A.CallTo(() => repository.GetMessageByIdAsync(message.MessageId, message.LanguageId)).Returns(message);

            var messageRezult = await service.GetMessageAsync(message.MessageId, message.LanguageId);
            Assert.Equal(message.MessageId, messageRezult.MessageId);
            if (string.IsNullOrEmpty(message.MessageId))
                await Assert.ThrowsAsync<Exception>(() => service.GetMessageAsync(message.MessageId, message.LanguageId));
        }

        [Theory(DisplayName = "Service get values test.")]
        [MemberData(nameof(Data.GetListFindReplace), MemberType = typeof(Data))]
        public async Task GetValuesTest(List<FindReplace> findReplace)
        {
            (var service, var repository) = GetMessagesService();

            var findReplaceRezult = await service.GetValuesAsync(findReplace);
            Assert.NotNull(findReplaceRezult);
            Assert.NotEmpty(findReplaceRezult);
            Assert.Equal(findReplaceRezult.FirstOrDefault().Find, findReplace.FirstOrDefault().Find);
        }

        [Theory(DisplayName = "Service get message push test.")]
        [MemberData(nameof(Data.GetMessageListFindReplace), MemberType = typeof(Data))]
        public async Task GetMessagePushTest(Message message, List<FindReplace> findReplace)
        {
            (var service, var repository) = GetMessagesService();
            A.CallTo(() => repository.GetMessageByIdAsync(message.MessageId, message.LanguageId)).Returns(message);

            var messageRezult = await service.GetMessagePushAsync(message.MessageId, message.LanguageId, findReplace);
            Assert.Equal(message.Subject, messageRezult.Subject);
            Assert.Equal(message.ContextText, messageRezult.Text);
            Assert.Equal(message.PreView, messageRezult.Title);
        }

        [Theory(DisplayName = "Service get message sms test.")]
        [MemberData(nameof(Data.GetMessageListFindReplace), MemberType = typeof(Data))]
        public async Task GetMessageSmsTest(Message message, List<FindReplace> findReplace)
        {
            (var service, var repository) = GetMessagesService();
            A.CallTo(() => repository.GetMessageByIdAsync(message.MessageId, message.LanguageId)).Returns(message);

            var messageRezult = await service.GetMessageSmsAsync(message.MessageId, message.LanguageId, findReplace);
            Assert.Equal(message.ContextSms, messageRezult.Text);
        }

        [Theory(DisplayName = "Service get message email test.")]
        [MemberData(nameof(Data.GetMessageFindReplaceTemplate), MemberType = typeof(Data))]
        public async Task GetMessageEmailAsyncTest(Message message, List<FindReplace> findReplace, Template template)
        {
            (var service, var repository) = GetMessagesService();
            A.CallTo(() => repository.GetTemplateByIdAsync(message.TemplateId, message.LanguageId)).Returns(template);
            A.CallTo(() => repository.GetMessageByIdAsync(message.MessageId, message.LanguageId)).Returns(message);

            var messageRezult = await service.GetMessageEmailAsync(message.MessageId, message.LanguageId, findReplace);
            Assert.Equal(message.Subject, messageRezult.Subject);
            Assert.Equal(message.ContextText, messageRezult.Text);
            Assert.Equal(template?.Header + message.ContextHtml + template?.Footer, messageRezult.Html);
        }

        [Theory(DisplayName = "Service get message email test.")]
        [MemberData(nameof(Data.GetMessageTemplate), MemberType = typeof(Data))]
        public async Task GetMessageForEmailTest(Message message, Template template)
        {
            (var service, var repository) = GetMessagesService();
            A.CallTo(() => repository.GetTemplateByIdAsync(message.TemplateId, message.LanguageId)).Returns(template);
            A.CallTo(() => repository.GetMessageByIdAsync(message.MessageId, message.LanguageId)).Returns(message);

            var messageRezult = await service.GetMessageForEmailAsync(message.MessageId, message.LanguageId);
            Assert.Equal(message.Subject, messageRezult.Subject);
            Assert.Equal(message.ContextText, messageRezult.Text);
            Assert.Equal(template?.Header + message.ContextHtml + template?.Footer, messageRezult.Html);
        }

        [Theory(DisplayName = "Service get message sms test.")]
        [MemberData(nameof(Data.GetMessage), MemberType = typeof(Data))]
        public async Task GetMessageForSmsTest(Message message)
        {
            (var service, var repository) = GetMessagesService();
            A.CallTo(() => repository.GetMessageByIdAsync(message.MessageId, message.LanguageId)).Returns(message);

            var messageRezult = await service.GetMessageForSmsAsync(message.MessageId, message.LanguageId);
            Assert.Equal(message.ContextSms, messageRezult.Text);
        }

        [Theory(DisplayName = "Service get message push test.")]
        [MemberData(nameof(Data.GetMessage), MemberType = typeof(Data))]
        public async Task GetMessageForPushTest(Message message)
        {
            (var service, var repository) = GetMessagesService();
            A.CallTo(() => repository.GetMessageByIdAsync(message.MessageId, message.LanguageId)).Returns(message);

            var messageRezult = await service.GetMessageForPushAsync(message.MessageId, message.LanguageId);
            Assert.Equal(message.Subject, messageRezult.Subject);
            Assert.Equal(message.ContextText, messageRezult.Text);
            Assert.Equal(message.PreView, messageRezult.Title);
        }
    }
}