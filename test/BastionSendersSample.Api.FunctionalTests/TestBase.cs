﻿using Bastion.Messages;
using Bastion.Messages.Options;
using Bastion.Messages.Stores;

using FakeItEasy;

using Microsoft.Extensions.Options;

namespace BastionSendersSample.Api.FunctionalTests
{
    public class TestBase
    {
        protected (MessagesService service, IMessagesRepository repository) GetMessagesService()
        {
            var repository = A.Fake<IMessagesRepository>();
            var userManager = A.Fake<IOptions<MessagesOptions>>();
            var service = new MessagesService(repository, userManager);
            return (service, repository);
        }
    }
}