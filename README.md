# Bastion Senders


# Messages services migration
```csharp
services.AddDbContext<MessagesDbContext>(options => options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection")));
```

## Messages migration
```
Add-Migration InitialCreate
Update-Database
```

# Send email sample with messages service

Startup.cs
```csharp
// ===== Add Bastion SendGrid email sender ========
services.AddSendGridEmailSender(Configuration);

// ===== Add Bastion Messages DbContext ========
services.AddDbContext<MessagesDbContext>(options 
    => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
services.AddTransient<IMessagesRepository, MessagesRepository>();
services.AddTransient<IMessagesService, MessagesService>();
```

Send email by message id
```csharp
await emailSender.SendEmailAsync(user.Email, messagesServices, "WELCOME", new
{
    UserName = user.Username
});
```